<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $guarded = [
    	"id", "user_id", "created_at", "updated_at"
    ];

    public function ownerUser(){
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function notices(){
    	return $this->hasMany('App\Notice')->orderBy('created_at', 'ASC');
    }
}
