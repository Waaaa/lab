<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    public function Logs(){
        return $this->hasMany('App\Log');
    }
}
