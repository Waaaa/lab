<?php

namespace App\Events;

use Auth;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AnnouncementPublishedForSomeone implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $announcement;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $announcement)
    {
        $this->user = $user;
        $this->announcement = $announcement;
    }

    public function broadcastWith()
    {
        return [
            'id' => $this->announcement->id,
            'title' => $this->announcement->title,
            'content' => nl2br($this->announcement->content),
            'publisher' => $this->announcement->ownerUser->name,
            'published_at' => date('Y-m-d H:i', strtotime($this->announcement->created_at)),
            'confirmURL' => route('announcement.confirm', ['announcement_id' => $this->announcement->id])
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        //return new PrivateChannel('channel-name');
        //return new PrivateChannel('users.1');
        return new Channel('users.'.$this->user->id);
    }
}
