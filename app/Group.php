<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

	protected $guarded = [
    	"id", "user_id", "created_at"
    ];
    public function members(){
        return $this->hasMany('App\GroupMember')->orderBy('created_at', 'ASC');
    }

    public function leader(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function isSuperAdminGroup(){
    	return $this->id == $this->getSuperAdminGroupId();
    }

    public function isAdminGroup(){
    	return $this->id == $this->getAdminGroupId();
    }

    public function isGuestGroup(){
    	if(!$this->isSuperAdminGroup() && !$this->isAdminGroup()){
    		return True;
    	}else{
    		return False;
    	}
    }

    public static function getSuperAdminGroupId(){
        return 1;
    }

    public static function getAdminGroupId(){
        return 2;
    }

    
}
