<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
	protected $table = "group_members";
    public $timestamps = false;
    protected $guarded = [
    	"id", "user_id", "group_id", "created_at"
    ];

    public function ownerGroup(){
        return $this->belongsTo('App\Group', 'group_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
