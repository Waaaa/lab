<?php

namespace App\Http\Controllers;

use Auth;
use Gate;
use URL;
use Session;
use DB;
use App\Announcement;
use App\Notice;
use App\Events\AnnouncementPublished;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Events\AnnouncementPublishedForSomeone;


class AnnouncementController extends Controller
{
    public function __construct()
	{

		
	}

	public function index()
	{
		$announcements = Announcement::orderBy('top', 'desc')->orderBy('created_at', 'desc')->paginate(10);

		//計算頁數
		if($announcements->lastPage() < $announcements->currentPage()){
			abort(404,'頁面不存在');
			exit;
		}


		return view('announcement.index', ['announcements' => $announcements]);
	}

	public function create(){
		//權限判斷
        if(Gate::denies('create', Announcement::class)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('announcement.index');
        }

		return view('announcement.create');
	}

	public function store(Request $request){
		//權限判斷
        if(Gate::denies('create', Announcement::class)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('announcement.index');
        }

		$input = $request->all();
		$rules = [
			'title' => 'required|string|max:50',
			'top' => 'nullable|integer|size:1',
			'content' => 'nullable|string',
		];

        $validator = Validator::make($input, $rules);

        if (!$validator->passes())
	    {
	        return redirect()->route('announcement.create')->withErrors($validator)->withInput();
	    }

	    DB::beginTransaction();
        try{
		    $announcement = new Announcement();
		    $announcement->title = $request->input('title');
		    if($request->has('top')){
		    	$announcement->top = 1;
		    }else{
		    	$announcement->top = 0;
		    }
		    $announcement->content = $request->input('content');
		    $announcement->user_id = Auth::user()->id;
		    $announcement->save();
			Session::flash('successMessage', '新增公告成功');
        	DB::commit();
        	
        }catch(Exception $e){
        	DB::rollback();
	    	Session::flash('errorMessage', '新增公告失敗 請稍後再試或聯絡網站管理員');
	    	return redirect()->route('announcement.index');
        }

        try{		    
		    if($request->has('broadcast') && $request->input('broadcast') == 1){
        		/* 廣播給除了自己外的所有人 */
        		broadcast(new AnnouncementPublished($announcement))->toOthers();
        	}
        }catch(Exception $e){
	    	Session::flash('errorMessage', '推播公告失敗 請稍後再試或聯絡網站管理員');
        }

        return redirect()->route('announcement.index');
	}

	public function show($announcement_id)
	{
		$announcement = Announcement::findOrFail($announcement_id);

		return view('announcement.show', ['announcement' => $announcement]);
	}

	public function edit($announcement_id){
		$announcement = Announcement::findOrFail($announcement_id);

		//權限判斷
        if(Gate::denies('update', $announcement)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('announcement.index');
        }

		return view('announcement.edit', ['announcement' => $announcement]);
	}

	public function update(Request $request, $announcement_id){
		$announcement = Announcement::findOrFail($announcement_id);

		//權限判斷
        if(Gate::denies('update', $announcement)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('announcement.index');
        }

		$input = $request->all();
		$rules = [
			'title' => 'required|string|max:50',
			'top' => 'nullable|integer|size:1',
			'content' => 'nullable|string',
		];

        $validator = Validator::make($input, $rules);

        if (!$validator->passes())
	    {
	        return redirect()->route('announcement.edit')->withErrors($validator)->withInput();
	    }

	    DB::beginTransaction();
        try{
		    
		    $announcement->title = $request->input('title');
		    if($request->has('top')){
		    	$announcement->top = 1;
		    }else{
		    	$announcement->top = 0;
		    }
		    $announcement->content = $request->input('content');
		    $announcement->user_id = Auth::user()->id;
		    $announcement->save();
			Session::flash('successMessage', '修改公告成功');
        	DB::commit();

        	
        }catch(Exception $e){
        	DB::rollback();
	    	Session::flash('errorMessage', '修改公告失敗 請稍後再試或聯絡網站管理員');
	    	return redirect()->route('announcement.index');
        }

        try{		    
		    if($request->has('broadcast') && $request->input('broadcast') == 1){
        		/* 廣播給除了自己外的所有人 */
        		broadcast(new AnnouncementPublished($announcement))->toOthers();
        	}
        }catch(Exception $e){
	    	Session::flash('errorMessage', '推播公告失敗 請稍後再試或聯絡網站管理員');
        }

        return redirect()->route('announcement.index');
	}

	public function destroy($announcement_id){
		$announcement = Announcement::findOrFail($announcement_id);

		//權限判斷
        if(Gate::denies('delete', $announcement)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('announcement.index');
        }

		DB::beginTransaction();
        try{		    
        	$notices = $announcement->notices;
        	foreach ($notices as $notice) {
        		$notice->delete();
        	}
		    $announcement->delete();
			Session::flash('successMessage', '刪除公告成功');
        	DB::commit();
        }catch(Exception $e){
        	DB::rollback();
	    	Session::flash('errorMessage', '刪除公告失敗 請稍後再試或聯絡網站管理員');
        }
        return redirect()->route('announcement.index');
	}

	//推播通知
	public function notice($announcement_id){
		$announcement = Announcement::findOrFail($announcement_id);
		
		//權限判斷
		if(Gate::denies('broadcast', Announcement::class)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('announcement.index');
        }
        
        try{		    
		    broadcast(new AnnouncementPublished($announcement))->toOthers();
        }catch(Exception $e){
	    	Session::flash('errorMessage', '推播公告失敗 請稍後再試或聯絡網站管理員');
        }
        return redirect()->route('announcement.index');

	}

	//已推播通知，從資料庫中刪除紀錄
	public function confirm($announcement_id){
		
		$announcement = Announcement::findOrFail($announcement_id);
        $notice = Notice::where('user_id', Auth::user()->id)->where('announcement_id', $announcement_id)->first();
        $notice->delete();

	}
}
