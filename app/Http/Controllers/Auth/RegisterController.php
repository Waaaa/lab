<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\JobTitle;
use Hash;
use Session;
use Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/users';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $jobTitlesId = implode(',', JobTitle::select('id')->pluck('id')->toArray());
        return Validator::make($data, [
            'student_id' => 'required|string|max:20|unique:users',
            'name' => 'required|string|max:20',
            'password' => 'required|string|min:6|confirmed',
            'title' => 'required|in:'.$jobTitlesId,
            'email' => 'nullable|string|email|max:100',
            'phone' => 'nullable|string|size:11',
            'birth' => 'nullable|date',
        ],[
            'required'    => '此欄位為必填',
            'unique'    => '已經有重複的帳號',
            'string'    => '請輸入文字',
            'max' => '輸入超出限定範圍，最大 :max 字元',
            'in'      => '請選擇有效的選項',
            'min'      => '最少要 :min 字元',
            'confirmed'      => '兩次輸入的內容不符',
            'email' => '請輸入格式正確的電子信箱',
            'phone' => '請輸入格式正確的連絡電話',
            'birth' => '請輸入格式正確的生日',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'student_id' => $data['student_id'],
            'name' => $data['name'],
            'password' => Hash::make($data['password']),
            'title_id' => $data['title'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'birth' => $data['birth'],
            'permission' => 1
        ]);
    }

    public function showRegistrationForm()
    {   
        //權限判斷
        if(Gate::denies('create', User::class)){
            abort(403);
        }

        $jobTitles = JobTitle::orderBy('id', 'DESC')->get();
        return view('users.create', ['jobTitles' => $jobTitles]);
    }

    public function register(Request $request)
    {
        //權限判斷
        if(Gate::denies('create', User::class)){
            abort(403);
        }
        
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    protected function registered(Request $request, $user)
    {
        Session::flash('successMessage', "使用者: ".$user->name." 新增成功");
    }
}
