<?php

namespace App\Http\Controllers;

use App\Log;
use Auth;
use Gate;
use Session;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class CalendarController extends Controller
{
    public function __construct()
	{

	}

	public function redirect(){
		$year = date('Y');
		$month = date('m');
		$day = date('d');
		return redirect()->route('calendar.month.log.list', ['type' => 'lab', 'year' => $year, 'month' => $month, 'day' => $day]);
	}

	public function index($type='lab', $year='', $month=''){

		if($type != 'lab' && $type != 'personal'){
			abort(404);
			exit;
		}

		$year = !empty($year) ? $year : date('Y');
		$month = !empty($month) ? $month : date('m');
		$day = "1";

		$tmp_day = ($day == "") ? "1" : $day;

		//日期驗證
		if(!checkdate($month, $tmp_day, $year)){
			return redirect()->route('calendar.month.index', ['type' => $type, 'year' => date('Y'), 'month' => date('m')]);
		    exit;
		}

		//該月有幾天
		$days = date('t',strtotime("{$year}-{$month}"));
		//該月1日是星期幾
		$week = date('w',strtotime("{$year}-{$month}-1"));

		if($month==1){
			$prevyear=$year-1;
			$prevmonth=12;
		}else{
			$prevyear=$year;
			$prevmonth=$month-1;
		}
		if($month==12){
			$nextyear=$year+1;
			$nextmonth=1;
		}else{
			$nextyear=$year;
			$nextmonth=$month+1;
		}

		//切換個人或全實驗室工作日誌
		$calendarTitle = ( $type == "personal" ) ? "個人工作日誌" : "E527 實驗室工作日誌" ;
		$icon = ( $type == "personal" ) ? '<i class="fas fa-flask"></i>' : '<i class="fas fa-user"></i>' ;
		$switchTypeLink = ( $type == "personal" ) ? route('calendar.month.log.list', ['type' => 'lab', 'year' => $year, 'month' => $month, 'day' => $day]) : route('calendar.month.log.list', ['type' => 'personal', 'year' => $year, 'month' => $month, 'day' => $day]) ;


		$preLink = route('calendar.month.index', ['type' => $type, 'year' => $prevyear, 'month' => $prevmonth]);
		$nextLink = route('calendar.month.index', ['type' => $type,'year' => $nextyear, 'month' => $nextmonth]);

		$tmp_log = new Log();
		return view('calendar.index', ['year' => $year, 'month' => $month, 'day' => $day, 'days' => $days, 'week' => $week, 'calendarTitle' => $calendarTitle, 'preLink' => $preLink, 'nextLink' => $nextLink, 'tmp_log' => $tmp_log, 'icon' => $icon, 'type' => $type, 'switchTypeLink' => $switchTypeLink]);
	}

	public function list($type='lab', $year='', $month='', $day=''){

		if($type != 'lab' && $type != 'personal'){
			abort(404);
			exit;
		}

		$year = !empty($year) ? $year : date('Y');
		$month = !empty($month) ? $month : date('m');
		$day = !empty($day) ? $day : date('m');

		$tmp_day = ($day == "") ? "1" : $day;

		//日期驗證
		if(!checkdate($month, $tmp_day, $year)){
			return redirect()->route('calendar.month.index', ['type' => $type, 'year' => date('Y'), 'month' => date('m')]);
		    exit;
		}

		//該月有幾天
		$days = date('t',strtotime("{$year}-{$month}"));
		//該月1日是星期幾
		$week = date('w',strtotime("{$year}-{$month}-1"));

		if($month==1){
			$prevyear=$year-1;
			$prevmonth=12;
		}else{
			$prevyear=$year;
			$prevmonth=$month-1;
		}
		if($month==12){
			$nextyear=$year+1;
			$nextmonth=1;
		}else{
			$nextyear=$year;
			$nextmonth=$month+1;
		}

		//切換個人或全實驗室工作日誌
		$calendarTitle = ( $type == "personal" ) ? "個人工作日誌" : "E527 實驗室工作日誌" ;
		$icon = ( $type == "personal" ) ? '<i class="fas fa-flask"></i>' : '<i class="fas fa-user"></i>' ;
		$switchTypeLink = ( $type == "personal" ) ? route('calendar.month.log.list', ['type' => 'lab', 'year' => $year, 'month' => $month, 'day' => $day]) : route('calendar.month.log.list', ['type' => 'personal', 'year' => $year, 'month' => $month, 'day' => $day]) ;

		$preLink = route('calendar.month.index', ['type' => $type, 'year' => $prevyear, 'month' => $prevmonth]);
		$nextLink = route('calendar.month.index', ['type' => $type,'year' => $nextyear, 'month' => $nextmonth]);

		$tmp_log = new Log();
		$logs = $tmp_log->getLogsByDate("$year-$month-$day", $type);
		return view('calendar.showLogList', ['year' => $year, 'month' => $month, 'day' => $day, 'days' => $days, 'week' => $week, 'calendarTitle' => $calendarTitle, 'preLink' => $preLink, 'nextLink' => $nextLink, 'tmp_log' => $tmp_log, "logs" => $logs, 'type' => $type, 'icon' => $icon, 'switchTypeLink' => $switchTypeLink]);
	}


}
