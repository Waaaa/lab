<?php

namespace App\Http\Controllers;

use App\Log;
use App\Group;
use App\GroupMember;
use App\JobTitle;
use App\User;
use Auth;
use Gate;
use Session;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class GroupController extends Controller
{
    public function __construct()
	{

		
	}

	public function index(){

		$groups = Group::paginate(10);
        $type = 'lab';
        
		//計算頁數
		if($groups->lastPage() < $groups->currentPage()){
			abort(404,'頁面不存在');
			exit;
		}
		
		return view('groups.index', ['groups' => $groups, 'type' => $type]);
	}

    public function personal(){

        $groupsId = Auth::user()->groups->pluck('group_id')->toArray();
        $groups = Group::whereIn('id', $groupsId)->paginate(10);
        $type = 'personal';

        //計算頁數
        if($groups->lastPage() < $groups->currentPage()){
            abort(404,'頁面不存在');
            exit;
        }
        
        return view('groups.index', ['groups' => $groups, 'type' => $type]);
    }

	public function create(){
		$users = User::get();
		return view('groups.create', ['users' => $users]);
	}

	public function store(Request $request){
		$input = $request->all();
		$usersId = implode(',', User::select('id')->pluck('id')->toArray());
		$rules = [
			'name' => 'required|string|max:50',
			'description' => 'nullable|string',
            'leader_id' => 'required|in:'.$usersId,
            'members' => 'nullable'
		];
		$message = [
            'required'    => '此欄位為必填',
            'string'    => '請輸入文字',
            'max' => '輸入超出限定範圍，最大 :max 字元',
            'in'      => '請選擇有效的選項',
            'min'      => '最少要 :min 字元',
        ];

        $validator = Validator::make($input, $rules, $message);
        if(!$validator->passes()){
        	return redirect()->route('group.create')->withErrors($validator)->withInput();
        }

        DB::beginTransaction();
        try{
        	$group = new Group();
        	$group->name = $request->input('name');
        	$group->description = $request->input('description');
        	$group->user_id = $request->input('leader_id');
        	$group->save();

        	$member = new GroupMember();
    		$member->group_id = $group->id;
    		$member->user_id = $group->user_id;
    		$member->created_at = date('Y-m-d H:i:s', time());
    		$member->save();

    		$members = $request->input('members');
    		if(isset($members)){
    			foreach ($members as $user_id) {
	        		if($user_id != $group->user_id){
		        		$member = new GroupMember();
		        		$member->group_id = $group->id;
		        		$member->user_id = $user_id;
		        		$member->created_at = date('Y-m-d H:i:s', time());
		        		$member->save();
	        		}
	        	}
    		}

        	Session::flash('successMessage', '新增群組: '.$group->name.' 成功');
        	DB::commit();
        }catch(Exception $e){
        	DB::rollback();
	    	Session::flash('errorMessage', '新增群組: $group->name 失敗 請稍後再試或聯絡網站管理員');

        }
        return redirect()->route('group.index');

	}

	public function show($group_id){
		$group = Group::findOrFail($group_id);

        //群組相關的工作日誌
        $logs = Log::where('group_id', $group_id)->orderBy('created_at', 'desc')->paginate(5);
        //計算頁數
        if($logs->lastPage() < $logs->currentPage()){
            abort(404,'頁面不存在');
            exit;
        }
		return view('groups.show', ['group' => $group, 'logs' => $logs]);
	}

	public function edit($group_id){
		$group = Group::findOrFail($group_id);

		//權限判斷
        if(Gate::denies('update', $group)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('group.index');
        }

		$users = User::get();
		$membersUserId = $group->members()->select('user_id')->pluck('user_id')->toArray();
		return view('groups.edit', ['group' => $group, 'users' => $users, 'membersUserId' => $membersUserId]);
	}

	public function update(Request $request, $group_id){
		$group = Group::findOrFail($group_id);

		//權限判斷
        if(Gate::denies('update', $group)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('group.index');
        }

		$input = $request->all();
		$usersId = implode(',', User::select('id')->pluck('id')->toArray());
		$rules = [
			'name' => 'required|string|max:50',
			'description' => 'nullable|string',
            'leader_id' => 'required|in:'.$usersId,
            'members' => 'nullable'
		];
		$message = [
            'required'    => '此欄位為必填',
            'string'    => '請輸入文字',
            'max' => '輸入超出限定範圍，最大 :max 字元',
            'in'      => '請選擇有效的選項',
            'min'      => '最少要 :min 字元',
        ];

        $validator = Validator::make($input, $rules, $message);

	    if (!$validator->passes())
	    {
	        return redirect()->route('group.edit', ["group_id" => $group_id])->withErrors($validator)->withInput();
	    }

        DB::beginTransaction();
        try{
        	$group->name = $request->input('name');
        	$group->description = $request->input('description');
        	$group->user_id = $request->input('leader_id');
        	$group->save();

        	$members = $group->members;
    		foreach ($members as $member) {
    			$member->delete();
    		}

        	$member = new GroupMember();
    		$member->group_id = $group->id;
    		$member->user_id = $group->user_id;
    		$member->created_at = date('Y-m-d H:i:s', time());
    		$member->save();

    		$members = $request->input('members');
    		if(isset($members)){
    			foreach ($members as $user_id) {
	        		if($user_id != $group->user_id){
		        		$member = new GroupMember();
		        		$member->group_id = $group->id;
		        		$member->user_id = $user_id;
		        		$member->created_at = date('Y-m-d H:i:s', time());
		        		$member->save();
		        	}
	        	}
    		}

        	Session::flash('successMessage', '更新群組: '.$group->name.' 成功');
        	DB::commit();
        }catch(Exception $e){
        	DB::rollback();
	    	Session::flash('errorMessage', '更新群組: '.$group->name.' 失敗 請稍後再試或聯絡網站管理員');

        }
        return redirect()->route('group.index');
	}

	public function destroy($group_id){
		$group = Group::findOrFail($group_id);

		//權限判斷
        if(Gate::denies('delete', $group)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('group.index');
        }

		DB::beginTransaction();
        try{
        	$members = $group->members;
    		foreach ($members as $member) {
    			$member->delete();
    		}
    		$group->delete();
    		Session::flash('successMessage', '刪除群組: '.$group->name.' 成功');
        	DB::commit();
        }catch(Exception $e){
        	DB::rollback();
	    	Session::flash('errorMessage', '刪除群組: '.$group->name.' 失敗 請稍後再試或聯絡網站管理員');
        }

        return redirect()->route('group.index');
	}
}
