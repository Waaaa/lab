<?php

namespace App\Http\Controllers;

use App\Log;
use App\Color;
use App\Group;
use App\GroupMember;
use Auth;
use Gate;
use URL;
use Session;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class LogController extends Controller
{
    public function __construct()
	{

		
	}

	public function create(){
		$arr_url = explode('/', URL::previous());
		if(in_array('personal', $arr_url)){
		  $type = "personal";
		}else{
		  $type = "lab";
		}

		$colors = Color::get();

		//最高管理者和管理者可以建立所有群組的工作日誌
		if(Auth::user()->isSuperAdmin()){
			$groups = array();
			$arr_group_id = Group::get()->pluck('id')->toArray();
			$user_id = Auth::user()->id;
			foreach ($arr_group_id as $group_id) {
				$group = new GroupMember();
				$group->group_id = $group_id;
				$group->user_id = $user_id;
				$groups[] = $group;
			}
		}elseif(Auth::user()->isAdmin()){
			$groups = array();
			$arr_group_id = Group::where('id', '<>', Group::getSuperAdminGroupId())->pluck('id')->toArray();
			$user_id = Auth::user()->id;
			foreach ($arr_group_id as $group_id) {
				$group = new GroupMember();
				$group->group_id = $group_id;
				$group->user_id = $user_id;
				$groups[] = $group;
			}
		}else{
			$groups = Auth::user()->groups;	
		}
		
		
		return view('logs.create', ['type' => $type, 'colors' => $colors, 'groups' => $groups]);
	}

	public function store(Request $request){
		$arr_url = explode('/', URL::previous());
		if(in_array('personal', $arr_url)){
		  $type = "personal";
		}else{
		  $type = "lab";
		}

		$input = $request->all();
		$colorsId = implode(',', Color::select('id')->pluck('id')->toArray());

		//最高管理者和管理者可以建立所有群組的工作日誌
		if(Auth::user()->isSuperAdmin()){
			$groupsId = implode(',', Group::get()->pluck('id')->toArray());
		}elseif(Auth::user()->isAdmin()){
			$groupsId = implode(',', Group::where('id', '<>', Group::getSuperAdminGroupId())->pluck('id')->toArray());
		}else{
			$groupsId = implode(',', Auth::user()->groups->pluck('group_id')->toArray());
		}	

		$rules = [
			'title' => 'required|string|max:50',
			'startDate' => 'required|date',
			'startTime' => 'required|date_format:H:i',
			'endDate' => 'required|date',
			'endTime' => 'required|date_format:H:i',
			'color_id' => 'required|in:'.$colorsId,
			'group_id' => 'nullable|in:0,'.$groupsId,
			'content' => 'nullable|string',
		];
		$message = [
            'required'    => '此欄位為必填',
            'string'    => '請輸入文字',
            'max' => '輸入超出限定範圍，最大 :max 字元',
            'in'      => '請選擇有效的選項',
            'min'      => '最少要 :min 字元',
            'date' => '請輸入有效的日期',
            'date_format' => '請輸入有效的時間',
        ];
        $validator = Validator::make($input, $rules, $message);

        if (!$validator->passes())
	    {
	    	$colors = Color::get();
			$groups = Auth::user()->groups;
			if($validator->errors()->has('startDate') || $validator->errors()->has('startTime') || $validator->errors()->has('endDate') || $validator->errors()->has('endTime')){
				$validator->errors()->add('time', '請輸入有效的時間');
			}
	        return redirect()->route('log.create')->withErrors($validator)->withInput();
	    }

	    /* 判斷開始時間是否小於結束時間 */
	    if(strtotime($request->input('startDate').' '.$request->input('startTime')) >= strtotime($request->input('endDate').' '.$request->input('endTime'))){
	    	$validator->errors()->add('time', '請輸入有效的時間');
	    	return redirect()->route('log.create')->withErrors($validator)->withInput();
	    }

	    DB::beginTransaction();
        try{
        	$log = new Log();
        	$log->title = $request->input('title');
        	$log->content = $request->input('content');
        	$log->start_date = $request->input('startDate');
        	$log->start_time = $request->input('startTime');
        	$log->end_date = $request->input('endDate');
        	$log->end_time = $request->input('endTime');
        	$log->color_id = $request->input('color_id');
        	$log->user_id = Auth::user()->id;
        	$log->group_id = $request->input('group_id');
        	$log->save();

        	Session::flash('successMessage', '新增工作日誌成功');
        	DB::commit();
        }catch(Exception $e){
        	DB::rollback();
	    	Session::flash('errorMessage', '新增工作日誌失敗 請稍後再試或聯絡網站管理員');
        }

        $arr_date = explode("-", $log->start_date);
		$year = $arr_date[0];
		$month = $arr_date[1];
		$day = $arr_date[2];
        return redirect()->route('calendar.month.log.list', ['type' => $type, 'year' => $year, 'month' => $month, 'day' => $day]);
	}

	public function show($log_id){
		$log = Log::findOrFail($log_id);

		$arr_date = explode("-", $log->start_date);
		$year = $arr_date[0];
		$month = $arr_date[1];
		$day = $arr_date[2];

		$tmp_day = ($day == "") ? "1" : $day;

		//切換個人或全實驗室工作日誌
		$arr_url = explode('/', URL::previous());
		if(in_array('personal', $arr_url)){
		  $type = "personal";
		}else{
		  $type = "lab";
		}
		$calendarTitle = ( $type == "personal" ) ? "個人工作日誌" : "E527 實驗室工作日誌" ;
		$icon = ( $type == "personal" ) ? '<i class="fas fa-flask"></i>' : '<i class="fas fa-user"></i>' ;
		$switchTypeLink = ( $type == "personal" ) ? route('calendar.month.log.list', ['type' => 'lab', 'year' => $year, 'month' => $month, 'day' => $day]) : route('calendar.month.log.list', ['type' => 'personal', 'year' => $year, 'month' => $month, 'day' => $day]) ;

		//日期驗證
		if(!checkdate($month, $tmp_day, $year)){
			return redirect()->route('calendar.month.index', ['type' => $type, 'year' => date('Y'), 'month' => date('m'), 'day' => date('d')]);
		    exit;
		}

		//該月有幾天
		$days = date('t',strtotime("{$year}-{$month}"));
		//該月1日是星期幾
		$week = date('w',strtotime("{$year}-{$month}-1"));

		if($month==1){
			$prevyear=$year-1;
			$prevmonth=12;
		}else{
			$prevyear=$year;
			$prevmonth=$month-1;
		}
		if($month==12){
			$nextyear=$year+1;
			$nextmonth=1;
		}else{
			$nextyear=$year;
			$nextmonth=$month+1;
		}

		$preLink = route('calendar.month.index', ['type' => $type, 'year' => $prevyear, 'month' => $prevmonth]);
		$nextLink = route('calendar.month.index', ['type' => $type, 'year' => $nextyear, 'month' => $nextmonth]);

		//$content = str_replace("%8je89fsk3%", "<br />", htmlentities(str_replace("<br />", "%8je89fsk3%", $log->content)));

		$tmp_log = new Log();
		return view('logs.show', ['year' => $year, 'month' => $month, 'day' => $day, 'days' => $days, 'week' => $week, 'calendarTitle' => $calendarTitle, 'preLink' => $preLink, 'nextLink' => $nextLink, 'tmp_log' => $tmp_log, 'log' => $log, 'type' => $type, 'switchTypeLink' => $switchTypeLink, 'icon' => $icon]);

	}

	public function edit($log_id){
		$log = Log::findOrFail($log_id);

		$arr_url = explode('/', URL::previous());
		if(in_array('personal', $arr_url)){
		  $type = "personal";
		}else{
		  $type = "lab";
		}

		//工作日誌送出一週後即無法再更改內容
        if(strtotime($log->created_at." +1 week") < strtotime(date("Y-m-d"))){
        	$arr_date = explode("-", $log->start_date);
			$year = $arr_date[0];
			$month = $arr_date[1];
			$day = $arr_date[2];
			Session::flash('errorMessage', '工作日誌送出一週後即無法再更改內容 請聯絡網站管理員');
	        return redirect()->route('calendar.month.log.list', ['type' => $type, 'year' => $year, 'month' => $month, 'day' => $day]);
        }

        //權限判斷
        if(Gate::denies('update', $log)){
        	$arr_date = explode("-", $log->start_date);
			$year = $arr_date[0];
			$month = $arr_date[1];
			$day = $arr_date[2];
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('calendar.month.log.list', ['type' => $type, 'year' => $year, 'month' => $month, 'day' => $day]);
        }

		$colors = Color::get();
		//最高管理者和管理者可以建立所有群組的工作日誌
		if(Auth::user()->isSuperAdmin()){
			$groups = array();
			$arr_group_id = Group::get()->pluck('id')->toArray();
			$user_id = Auth::user()->id;
			foreach ($arr_group_id as $group_id) {
				$group = new GroupMember();
				$group->group_id = $group_id;
				$group->user_id = $user_id;
				$groups[] = $group;
			}
		}elseif(Auth::user()->isAdmin()){
			$groups = array();
			$arr_group_id = Group::where('id', '<>', Group::getSuperAdminGroupId())->pluck('id')->toArray();
			$user_id = Auth::user()->id;
			foreach ($arr_group_id as $group_id) {
				$group = new GroupMember();
				$group->group_id = $group_id;
				$group->user_id = $user_id;
				$groups[] = $group;
			}
		}else{
			$groups = Auth::user()->groups;	
		}
		return view('logs.edit', ['type' => $type, 'log' => $log, 'colors' => $colors, 'groups' => $groups]);
	}

	public function update(Request $request, $log_id){
		$log = Log::findOrFail($log_id);

		$arr_url = explode('/', URL::previous());
		if(in_array('personal', $arr_url)){
		  $type = "personal";
		}else{
		  $type = "lab";
		}

		//工作日誌送出一週後即無法再更改內容
        if(strtotime($log->created_at." +1 week") < strtotime(date("Y-m-d"))){
        	$arr_date = explode("-", $log->start_date);
			$year = $arr_date[0];
			$month = $arr_date[1];
			$day = $arr_date[2];
			Session::flash('errorMessage', '工作日誌送出一週後即無法再更改內容 請聯絡網站管理員');
	        return redirect()->route('calendar.month.log.list', ['type' => $type, 'year' => $year, 'month' => $month, 'day' => $day]);
        }

        //權限判斷
        if(Gate::denies('update', $log)){
        	$arr_date = explode("-", $log->start_date);
			$year = $arr_date[0];
			$month = $arr_date[1];
			$day = $arr_date[2];
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('calendar.month.log.list', ['type' => $type, 'year' => $year, 'month' => $month, 'day' => $day]);
        }

		$input = $request->all();
		$colorsId = implode(',', Color::select('id')->pluck('id')->toArray());

		//最高管理者和管理者可以建立所有群組的工作日誌
		if(Auth::user()->isSuperAdmin()){
			$groupsId = implode(',', Group::get()->pluck('id')->toArray());
		}elseif(Auth::user()->isAdmin()){
			$groupsId = implode(',', Group::where('id', '<>', Group::getSuperAdminGroupId())->pluck('id')->toArray());
		}else{
			$groupsId = implode(',', Auth::user()->groups->pluck('group_id')->toArray());
		}

		$rules = [
			'title' => 'required|string|max:50',
			'startDate' => 'required|date',
			'startTime' => 'required|date_format:H:i',
			'endDate' => 'required|date',
			'endTime' => 'required|date_format:H:i',
			'color_id' => 'required|in:'.$colorsId,
			'group_id' => 'nullable|in:0,'.$groupsId,
			'content' => 'nullable|string',
		];
		$message = [
            'required'    => '此欄位為必填',
            'string'    => '請輸入文字',
            'max' => '輸入超出限定範圍，最大 :max 字元',
            'in'      => '請選擇有效的選項',
            'min'      => '最少要 :min 字元',
            'date' => '請輸入有效的日期',
            'date_format' => '請輸入有效的時間',
        ];
        $validator = Validator::make($input, $rules, $message);

        if (!$validator->passes())
	    {
	    	$colors = Color::get();
			$groups = Auth::user()->groups;
			if($validator->errors()->has('startDate') || $validator->errors()->has('startTime') || $validator->errors()->has('endDate') || $validator->errors()->has('endTime')){
				$validator->errors()->add('time', '請輸入有效的時間');
			}
	        return redirect()->route('log.edit', ["log_id" => $log->id])->withErrors($validator)->withInput();
	    }

	    /* 判斷開始時間是否小於結束時間 */
	    if(strtotime($request->input('startDate').' '.$request->input('startTime')) >= strtotime($request->input('endDate').' '.$request->input('endTime'))){
	    	$validator->errors()->add('time', '請輸入有效的時間');
	    	return redirect()->route('log.edit', ["log_id" => $log->id])->withErrors($validator)->withInput();
	    }

	    DB::beginTransaction();
        try{
        	$log->title = $request->input('title');
        	$log->content = $request->input('content');
        	$log->start_date = $request->input('startDate');
        	$log->start_time = $request->input('startTime');
        	$log->end_date = $request->input('endDate');
        	$log->end_time = $request->input('endTime');
        	$log->color_id = $request->input('color_id');
        	$log->user_id = Auth::user()->id;
        	$log->group_id = $request->input('group_id');
        	$log->save();

        	Session::flash('successMessage', '修改工作日誌成功');
        	DB::commit();
        }catch(Exception $e){
        	DB::rollback();
	    	Session::flash('errorMessage', '修改工作日誌失敗 請稍後再試或聯絡網站管理員');
        }

        $arr_date = explode("-", $log->start_date);
		$year = $arr_date[0];
		$month = $arr_date[1];
		$day = $arr_date[2];
        return redirect()->route('calendar.month.log.list', ['type' => $type, 'year' => $year, 'month' => $month, 'day' => $day]);
	}

	public function destroy($log_id){
		$log = Log::findOrFail($log_id);

		$arr_url = explode('/', URL::previous());
		if(in_array('personal', $arr_url)){
		  $type = "personal";
		}else{
		  $type = "lab";
		}

		//權限判斷
        if(Gate::denies('delete', $log)){
        	$arr_date = explode("-", $log->start_date);
			$year = $arr_date[0];
			$month = $arr_date[1];
			$day = $arr_date[2];
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('calendar.month.log.list', ['type' => $type, 'year' => $year, 'month' => $month, 'day' => $day]);
        }

		DB::beginTransaction();
        try{
    		$log->delete();
    		Session::flash('successMessage', '刪除工作日誌: '.$log->title.' 成功');
        	DB::commit();
        }catch(Exception $e){
        	DB::rollback();
	    	Session::flash('errorMessage', '刪除工作日誌: '.$log->title.' 失敗 請稍後再試或聯絡網站管理員');
        }

        $arr_date = explode("-", $log->start_date);
		$year = $arr_date[0];
		$month = $arr_date[1];
		$day = $arr_date[2];
        return redirect()->route('calendar.month.log.list', ['type' => $type, 'year' => $year, 'month' => $month, 'day' => $day]);
	}

}
