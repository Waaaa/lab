<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\JobTitle;
use Auth;
use Gate;
use Hash;
use Session;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Events\AnnouncementPublishedForSomeone;


class UserController extends Controller
{
    public function __construct()
	{

		
	}

	public function index(){

		$users = User::orderBy('title_id', 'asc')->orderBy('created_at', 'asc')->paginate(10);

		//計算頁數
		if($users->lastPage() < $users->currentPage()){
			abort(404,'頁面不存在');
			exit;
		}
		return view('users.index', ['users' => $users]);
	}

	//展示個人資料頁面
	public function showProfileForm(){
		$jobTitles = JobTitle::get();
		$profileFieldStatus = "disabled='true'";
		return view('users.profile', ['jobTitles' => $jobTitles, 'profileFieldStatus' => $profileFieldStatus]);
	}

	//更新個人資料頁面
	public function updateProfile(Request $request){
		$user = User::findOrFail(Auth::user()->id);

		$input = $request->all();
		$jobTitlesId = implode(',', JobTitle::select('id')->pluck('id')->toArray());
		$rules = [
			'name' => 'required|string|max:20',
            'title' => 'required|in:'.$jobTitlesId,
            'email' => 'nullable|string|email|max:100',
            'phone' => 'nullable|string|size:11',
            'birth' => 'nullable|date',
		];
		$message = [
            'required'    => '此欄位為必填',
            'unique'    => '已經有重複的帳號',
            'string'    => '請輸入文字',
            'max' => '輸入超出限定範圍，最大 :max 字元',
            'in'      => '請選擇有效的選項',
            'min'      => '最少要 :min 字元',
            'confirmed'      => '兩次輸入的內容不符',
            'email' => '請輸入格式正確的電子信箱',
            'phone' => '請輸入格式正確的連絡電話',
            'birth' => '請輸入格式正確的生日',
        ];

        $validator = Validator::make($input, $rules, $message);
        if(!$validator->passes()){
        	return redirect()->route('user.profile')->withErrors($validator, 'profile')->withInput();

        }

        DB::beginTransaction();
        try{
        	
	        $user->name = $request->input('name');
	        $user->title_id = $request->input('title');
	        $user->phone = $request->input('phone');
	        $user->birth = $request->input('birth');
	        $user->save();

        	Session::flash('successMessage', '更新個人資料成功');
        	DB::commit();
        }catch(Exception $e){
        	DB::rollback();
	    	Session::flash('errorMessage', '更新個人資料失敗 請稍後再試或聯絡網站管理員');

        }
        return redirect()->route('user.profile');

	}

	//更新個人密碼
	public function updatePassword(Request $request){
		$user = User::findOrFail(Auth::user()->id);
		
		$input = $request->all();
		$rules = [
			'oldPassword' => 'required|string',
			'password' => 'required|string|min:6|confirmed'
		];
		$message = [
			'required'    => '此欄位為必填',
            'string'    => '請輸入文字',
            'min'      => '最少要 :min 字元',
            'confirmed'      => '兩次輸入的內容不符',
		];
		$validator = Validator::make($input, $rules, $message);
		if(!$validator->passes()){
			return redirect()->route('user.profile')->withErrors($validator, 'password');
		}

		if (Hash::check($request->input('oldPassword'), Auth::user()->password)) {
			$newPassword = Hash::make($request->input('password'));
			$user->password = $newPassword;
			$user->save();
			Auth::logout();
			Session::flash('successMessage', '密碼修改成功，請重新登入');
  			return redirect()->route('login');
		}else{
			$validator->errors()->add('oldPassword', '密碼錯誤');
			return redirect()->route('user.profile')->withErrors($validator, 'password');
		}
	}

	//重設他人密碼
	public function resetPassword($user_id){
		$user = User::findOrFail($user_id);

		//權限判斷
        if(Gate::denies('resetPassword', $user)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('user.index');
        }

		$newPassword = Hash::make($user->student_id.'@nkust');
		$user->password = $newPassword;
		$user->save();
		Session::flash('successMessage', "$user->student_id 密碼修改成功，已更新為: 學號@nkust");
		return redirect()->route('user.index');
	}

	//停權他人
	public function ban($user_id){
		$user = User::findOrFail($user_id);

		//權限判斷
        if(Gate::denies('banAndAllow', $user)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('user.index');
        }

		$user->permission = 0;
		$user->save();
		Session::flash('successMessage', "$user->student_id 已經停權");
		return redirect()->route('user.index');
	}

	//復權他人
	public function allow($user_id){
		$user = User::findOrFail($user_id);

		//權限判斷
        if(Gate::denies('banAndAllow', $user)){
            Session::flash('errorMessage', "權限不符，請聯絡網站管理員");
			return redirect()->route('user.index');
        }

		$user->permission = 1;
		$user->save();
		Session::flash('successMessage', "$user->student_id 已經復權");
		return redirect()->route('user.index');
	}

	public function returnUser(){
	    $user = Auth::user();
	    $userData = [
	    	"id" => $user->id
	    ];
	    return json_encode($userData);
	}

	public function checkNotice(){
		$user = Auth::user();
		$notices = $user->notices;
        foreach ($notices as $notice) {
            $announcement = $notice->announcements;
            broadcast(new AnnouncementPublishedForSomeone($user, $announcement));
        }
	}

	public function defaultData(){
		$arr_users = [
            [
                "name" => "漫威工作室",
                "student_id" => "MarvelStudios",
                "password" => "IAmMarvelStudios",
                "title_id" => 1,
            ],
            [
                "name" => "迪士尼",
                "student_id" => "Disney",
                "password" => "IAmDisney",
                "title_id" => 1,
            ],
        	[
        		"name" => "古一",
        		"student_id" => "TheAncientOne",
        		"password" => "IAmTheAncientOne",
        		"title_id" => 1,
        	],
        	[
        		"name" => "卡爾 莫度",
        		"student_id" => "KarlMordo",
        		"password" => "IAmKarlMordo",
        		"title_id" => 2,
        	],
        	[
        		"name" => "奇異博士",
        		"student_id" => "DoctorStrange",
        		"password" => "IAmDoctorStrange",
        		"title_id" => 3,
        	],
        	[
        		"name" => "王",
        		"student_id" => "Wong",
        		"password" => "IAmWong",
        		"title_id" => 4,
        	],
        	[
        		"name" => "美國隊長",
        		"student_id" => "CaptainAmerica",
        		"password" => "IAmCaptainAmerica",
        		"title_id" => 1,
        	],
        	[
        		"name" => "酷寒戰士",
        		"student_id" => "WinterSoldier",
        		"password" => "IAmWinterSoldier",
        		"title_id" => 4,
        	],
        	[
        		"name" => "緋紅女巫",
        		"student_id" => "ScarletWitch",
        		"password" => "IAmScarletWitch",
        		"title_id" => 4,
        	],
        	[
        		"name" => "鋼鐵人",
        		"student_id" => "IronMan",
        		"password" => "IAmIronMan",
        		"title_id" => 1,
        	],
        	[
        		"name" => "黑寡婦",
        		"student_id" => "BlackWidow",
        		"password" => "IAmBlackWidow",
        		"title_id" => 2,
        	],
        	[
        		"name" => "蜘蛛人",
        		"student_id" => "SpiderMan",
        		"password" => "IAmSpiderMan",
        		"title_id" => 4,
        	],
        ];

        $arr_groups = [
        	[
        		"name" => "sa",
        		"description" => "最高管理者群組。",
        		"user_id" => 1
        	],
        	[
        		"name" => "admin",
        		"description" => "管理者群組。",
        		"user_id" => 2
        	],
        	[
        		"name" => "卡瑪泰姬",
        		"description" => "位於尼泊爾加德滿都的魔法聖殿。",
        		"user_id" => 3
        	],
        	[
        		"name" => "反對派陣營",
        		"description" => "我反對R!",
        		"user_id" => 7
        	],
        	[
        		"name" => "贊成派陣營",
        		"description" => "我贊成R!",
        		"user_id" => 10
        	],
        ];

        $arr_groupMembers = [
        	[1],
        	[2],
        	[3, 4, 5, 6],
        	[7, 8, 9],
        	[10, 11, 12],
        ];


        $users = array();
        foreach ($arr_users as $user) {
        	$tmp_user = new User();
        	$tmp_user->name = $user["name"];
        	$tmp_user->student_id = $user["student_id"];
        	$tmp_user->password = $user["password"];
        	$tmp_user->title_id = $user["title_id"];
        	$users[] = $tmp_user;
        }

        return view('users.default', ["users" => $users, "arr_groups" => $arr_groups, "arr_groupMembers" => $arr_groupMembers]);
	}
	
}
