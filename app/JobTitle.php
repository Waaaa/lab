<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobTitle extends Model
{	
	protected $table = "job_titles";
    protected $guarded = [
    	"id"
    ];

    public function users(){
        return $this->hasMany('App\User', 'title_id')->orderBy('created_at', 'DESC');
    }
}
