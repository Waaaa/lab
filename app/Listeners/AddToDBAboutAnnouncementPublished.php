<?php

namespace App\Listeners;

use Auth;
use DB;
use App\User;
use App\Notice;
use App\Events\AnnouncementPublished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddToDBAboutAnnouncementPublished
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AnnouncementPublished  $event
     * @return void
     */
    public function handle(AnnouncementPublished $event)
    {
        $usersId = User::where('id', '<>', Auth::user()->id)->select('id')->pluck('id')->toArray();
        DB::beginTransaction();
        try{
            //新增通知進資料庫
            foreach ($usersId as $userId) {
                $notice = new Notice();
                $notice->user_id = $userId;
                $notice->announcement_id = $event->announcement->id;
                $notice->save();
            }
            DB::commit();
        }catch(Exception $e){
            DB::rollback();
        }
    }
}
