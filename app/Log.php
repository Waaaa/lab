<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    
    protected $guarded = [
    	"id", "user_id", "group_id", "created_at", "updated_at"
    ];

    public function ownerUser(){
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function ownerGroup(){
    	return $this->belongsTo('App\Group', 'group_id', 'id');
    }

    public function color(){
        return $this->belongsTo('App\Color', 'color_id', 'id');
    }

    public function getLogsByDate($date, $type){

        $groupsId = implode(',', Auth::user()->groups->pluck('group_id')->toArray());
        $user_id = Auth::user()->id;
        if($type == "personal"){
            return Log::WhereRaw("(group_id IN ($groupsId) OR user_id = $user_id) AND start_date = '$date'")->get();
        }else{
            return Log::where('start_date', date($date))->get();    
        }
        
    }

    //輸出包含小組名稱的標題
    public function getFullTitleAttribute(){

        if($this->ownerGroup !== NULL){
            $groupName = $this->ownerGroup->name;
            return "[$groupName] ".$this->title;
        }else{
            return $this->title;
        } 
    }
    
    public function getStartTimeAttribute($value){
        return date('H:i', strtotime($this->attributes['start_time']));        
    }

    public function getEndTimeAttribute($value){
        return date('H:i', strtotime($this->attributes['end_time']));        
    }


}
