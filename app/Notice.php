<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    public function announcements(){
        return $this->hasOne('App\Announcement', 'id', 'announcement_id');
    }
}
