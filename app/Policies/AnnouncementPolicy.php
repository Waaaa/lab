<?php

namespace App\Policies;

use App\User;
use App\Announcement;
use Illuminate\Auth\Access\HandlesAuthorization;

class AnnouncementPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function view(User $user, Announcement $announcement)
    {
        //
    }

    /**
     * Determine whether the user can create Announcements.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //一般管理者與最高管理者可以新增公告
        if($user->isSuperAdmin() || $user->isAdmin()){
            return True;
        }else{
            return False;
        }
    }

    /**
     * Determine whether the user can update the Announcement.
     *
     * @param  \App\User  $user
     * @param  \App\Announcement  $announcement
     * @return mixed
     */
    public function update(User $user, Announcement $announcement)
    {   
        //最高管理者可以更新所有公告
        if($user->isSuperAdmin()){
            return True;
        }
        
        //管理者可以更新自己的公告
        if($user->isAdmin() && $announcement->user_id == $user->id){
            return True;
        }
        return False;
    }

    /**
     * Determine whether the user can delete the Announcement.
     *
     * @param  \App\User  $user
     * @param  \App\Announcement  $announcement
     * @return mixed
     */
    public function delete(User $user, Announcement $announcement)
    {
        //最高管理者可以刪除所有公告
        if($user->isSuperAdmin()){
            return True;
        }
        
        //管理者可以刪除自己的公告
        if($user->isAdmin() && $announcement->user_id == $user->id){
            return True;
        }
        return False;
    }

    public function broadcast(User $user)
    {
        //一般管理者與最高管理者可以推播公告
        if($user->isSuperAdmin() || $user->isAdmin()){
            return True;
        }else{
            return False;
        }
    }
}
