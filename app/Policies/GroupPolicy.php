<?php

namespace App\Policies;

use App\User;
use App\Group;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the group.
     *
     * @param  \App\User  $user
     * @param  \App\Group  $group
     * @return mixed
     */
    public function view(User $user, Group $group)
    {
        //
    }

    /**
     * Determine whether the user can create groups.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        /*
        //一般管理者與最高管理者可以新增群組
        if($user->isSuperAdmin() || $user->isAdmin()){
            return True;
        }else{
            return False;
        }*/

        //開放一般使用者建立群組
        return True;
    }

    /**
     * Determine whether the user can update the group.
     *
     * @param  \App\User  $user
     * @param  \App\Group  $group
     * @return mixed
     */
    public function update(User $user, Group $group)
    {   
        //群組的組長可以更新自已的群組
        if($user->id == $group->user_id && (!$group->isAdminGroup())){
            return True;
        }

        //一般管理者可以更新所有的一般群組
        if($user->isAdmin() && $group->isGuestGroup()){
            return True;
        }

        //最高管理者所有群組
        if($user->isSuperAdmin()){
            return True;
        }

        return False;
    }

    /**
     * Determine whether the user can delete the group.
     *
     * @param  \App\User  $user
     * @param  \App\Group  $group
     * @return mixed
     */
    public function delete(User $user, Group $group)
    {
        //群組的組長可以刪除自已的群組
        if(($user->id == $group->user_id) && $group->isGuestGroup()){
            return True;
        }

        //一般管理者可以刪除所有的一般群組
        if($user->isAdmin() && $group->isGuestGroup()){
            return True;
        }

        //最高管理者可以刪除除了最高與一般管理者群組外的所有群組
        if($user->isSuperAdmin() && $group->isGuestGroup()){
            return True;
        }

        return False;
    }
}
