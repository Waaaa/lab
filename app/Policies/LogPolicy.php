<?php

namespace App\Policies;

use App\User;
use App\Log;
use Illuminate\Auth\Access\HandlesAuthorization;

class LogPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the log.
     *
     * @param  \App\User  $user
     * @param  \App\Log  $log
     * @return mixed
     */
    public function view(User $user, Log $log)
    {
        //
    }

    /**
     * Determine whether the user can create logs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //任何人都可以建立工作日誌
        return True;
    }

    /**
     * Determine whether the user can update the log.
     *
     * @param  \App\User  $user
     * @param  \App\Log  $log
     * @return mixed
     */
    public function update(User $user, Log $log)
    {   

        //只有建立工作日誌的使用者可以修改該工作日誌
        if($user->id == $log->user_id){
            return True;
        }else{
            return False;
        }
    }

    /**
     * Determine whether the user can delete the log.
     *
     * @param  \App\User  $user
     * @param  \App\Log  $log
     * @return mixed
     */
    public function delete(User $user, Log $log)
    {
        //建立工作日誌的使用者可以刪除該工作日誌
        if($user->id == $log->user_id){
            return True;
        }

        //該工作日誌群組的組長可以刪除該工作日誌
        if($user->id == @$log->ownerGroup->user_id){
            return True;
        }

        //管理者可以刪除工作日誌
        if(!$user->isGuest()){
            //最高管理者可以刪除所有日誌
            if($user->isSuperAdmin()){
                return True;
            }

            //管理者不能刪除最高管理者群組的日誌
            if($user->isAdmin() && !$log->ownerUser->isSuperAdmin()){
                if(isset($log->ownerGroup->id)){
                    if($log->ownerGroup->id != Group::getSuperAdminGroupId()){
                        return True;
                    }
                }else{
                    return True;
                }
            }
        }

        return False;
    }
}
