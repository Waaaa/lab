<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        //
    }

    //判斷是否可以新增使用者
    public function create(User $user)
    {   
        //一般管理者與最高管理者可以新增使用者
        if($user->isSuperAdmin() || $user->isAdmin()){
            return True;
        }else{
            return False;
        }
    }

    //判斷是否可以重設他人密碼
    public function resetPassword(User $user, User $model)
    {   
        //自己不能重設自己的密碼
        if($user->student_id == $model->student_id){
            return False;
        }

        if($user->isGuest() ){
            return False;
        }

       
        if($user->isAdmin() && !$model->isSuperAdmin() ){
             //一般管理者可以重設一般管理者與一般使用者的密碼
            return True;
        }elseif($user->isSuperAdmin() ){
            //最高管理者可以重設所有使用者的密碼
            return True;
        }else{
            return False;
        }
    }

    //判斷是否可以停/復權他人
    public function banAndAllow(User $user, User $model)
    {
        //自己不能停/復權自己
        if($user->student_id == $model->student_id){
            return False;
        }

        if($user->isGuest() ){
            return False;
        }

        //最高管理者不能被停/復權
        if($model->isSuperAdmin()){
            return False;
        }

        //一般管理者可以停/復權一般使用者
        if($user->isAdmin() && $model->isGuest() ){
            return True;
        }elseif($user->isSuperAdmin() && !$model->isSuperAdmin() ){
            return True;
        }else{
            return False;
        }
    }

}
