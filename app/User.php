<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Group;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function logs(){
        return $this->hasMany('App\Log', 'user_id')->orderBy('created_at', 'DESC');
    }

    public function groups(){
        return $this->hasMany('App\GroupMember', 'user_id')->orderBy('created_at', 'DESC');
    }

    public function jobTitle(){
        return $this->belongsTo('App\JobTitle', 'title_id', 'id');
    }

     public function notices(){
        return $this->hasMany('App\Notice', 'user_id')->orderBy('created_at', 'DESC');
    }

    public function isSuperAdmin(){
        $group = $this->groups()->where('group_id', Group::getSuperAdminGroupId())->get();
        if(count($group) > 0){
            return True;
        }else{
            return False;
        }
    }

    public function isAdmin(){
        $group = $this->groups()->where('group_id', Group::getAdminGroupId())->get();
        if(count($group) > 0){
            return True;
        }else{
            return False;
        }
    }

    public function isGuest(){
        if(!$this->isSuperAdmin() && !$this->isAdmin()){
            return True;
        }else{
            return False;
        }
    }

    //輸出身分
    public function getIdentityAttribute(){

        if($this->isSuperAdmin()){
            return "最高管理者";
        }elseif($this->isAdmin()){
            return "一般管理者";
        }else{
            return "一般使用者";
        }
    }


}
