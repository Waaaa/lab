<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //職稱資料表
        Schema::create('job_titles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
        });

        //使用者資料表
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('student_id', 20);
            $table->string('password', 255);
            $table->string('email', 254)->nullable();
            $table->string('phone', 20)->nullable();
            $table->integer('title_id')->foreign('title_id')->references('id')->on('job_titles');
            $table->date('birth')->nullable();
            $table->tinyinteger('permission')->nullable()->default(1);
            $table->timestamps();
            $table->string('remember_token', 255)->nullable();
        });

        //公告資料表
        Schema::create('announcements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 20);
            $table->text('content')->nullable();
            $table->tinyinteger('top')->nullable()->default(0);
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });

        //群組資料表
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->text('description')->nullable();
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });

        //群組成員資料表
        Schema::create('group_members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->foreign('group_id')->references('id')->on('groups');
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->datetime('created_at');
        });

        //顏色狀態資料表
        Schema::create('colors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('color', 7);
        });

        //工作日誌資料表
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 20);
            $table->text('content')->nullable();
            $table->date('start_date');
            $table->time('start_time');
            $table->date('end_date');
            $table->time('end_time');
            $table->integer('color_id')->foreign('color_id')->references('id')->on('colors');
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->integer('group_id')->nullable()->foreign('group_id')->references('id')->on('groups');
            $table->timestamps();
        });

        //通知資料表
        Schema::create('notices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->foreign('user_id')->references('id')->on('users');
            $table->integer('announcement_id')->foreign('announcement_id')->references('id')->on('announcements');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_titles');
        Schema::dropIfExists('users');
        Schema::dropIfExists('announcements');
        Schema::dropIfExists('groups');
        Schema::dropIfExists('group_members');
        Schema::dropIfExists('colors');
        Schema::dropIfExists('logs');
        Schema::dropIfExists('notices');
    }
}
