<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AnnouncementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_announcements = [
        	[
        		"title" => "安心出門，安心回家之奇異博士行車小心。",
        		"content" => "《奇異博士》（英語：Doctor Strange）是一部2016年美國超級英雄電影，改編自漫威漫畫旗下的同名漫畫人物。本片為漫威影業製作、華特迪士尼工作室電影發行，漫威電影宇宙的第十四部電影。由史考特·德瑞森執導，德瑞森、強·史派茲與C·羅柏·卡吉爾負責編劇。電影主演包括班奈狄克·康柏拜區、蒂妲·史雲頓、奇維托·艾吉佛、邁茲·米克森、黃凱旋、麥可·斯圖巴、班傑明·布萊特、史考特·艾金斯和瑞秋·麥亞當斯。",
        		"user_id" => 1
        	],
        	[
        		"title" => "大家要打起來囉!",
        		"content" => "在這部電影中，美國隊長和鋼鐵人因一項管理異能者活動的法案而為此態度對立，進而各自組建了陣營來對抗彼此。",
        		"user_id" => 1
        	],
        ];

        $arr_noticeMemberIds = [
        	[3, 4, 5, 6],
        	[7, 8, 9, 10, 11, 12],
        ];

        foreach ($arr_announcements as $index => $announcement) {
        	$id = $index+1;
        	DB::table('announcements')->insert([
	        	"title" => $announcement["title"],
	        	"content" => $announcement["content"],
	        	"user_id" => $announcement["user_id"],
	        	"created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
	        ]);

	        foreach ($arr_noticeMemberIds[$index] as $noticeMemberId) {
	        	DB::table('notices')->insert([
		        	"user_id" => $noticeMemberId,
		        	"announcement_id" => $id,
		        	"created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                    "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
		        ]);
	        }
        }
    }
}
