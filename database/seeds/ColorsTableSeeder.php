<?php

use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_colors = [
        	[
        		"name" => "普通",
        		"color" => "#4B515D"
        	],
        	[
        		"name" => "重要",
        		"color" => "#FF8800"
        	],
        	[
        		"name" => "緊急",
        		"color" => "#CC0000"
        	],
        ];

        $i = 0;
        foreach ($arr_colors as $color) {
        	$i += 1;
        	DB::table('colors')->insert([
	        	"name" => $color["name"],
	        	"color" => $color["color"],
	        ]);
        }
    }
}
