<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    protected $toTruncate = ["job_titles", "users", "group_members", "groups", "notices", "announcements", "colors", "logs"];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function(){
            foreach($this->toTruncate as $table) {
                DB::table($table)->truncate();
            }

        	$this->call(JobTitlesTableSeeder::class);
            $this->call(UsersTableSeeder::class);
            $this->call(GroupsTableSeeder::class);
            $this->call(AnnouncementsTableSeeder::class);
            $this->call(ColorsTableSeeder::class);
            $this->call(LogsTableSeeder::class);
        });
    }
}
