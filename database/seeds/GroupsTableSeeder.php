<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_groups = [
        	[
        		"name" => "sa",
        		"description" => "最高管理者群組。",
        		"user_id" => 1
        	],
        	[
        		"name" => "admin",
        		"description" => "管理者群組。",
        		"user_id" => 2
        	],
        	[
        		"name" => "卡瑪泰姬",
        		"description" => "位於尼泊爾加德滿都的魔法聖殿。",
        		"user_id" => 3
        	],
        	[
        		"name" => "反對派陣營",
        		"description" => "我反對R!",
        		"user_id" => 7
        	],
        	[
        		"name" => "贊成派陣營",
        		"description" => "我贊成R!",
        		"user_id" => 10
        	],
        ];

        $arr_groupMembers = [
        	[1],
        	[2],
        	[3, 4, 5, 6],
        	[7, 8, 9],
        	[10, 11, 12],
        ];

        foreach ($arr_groups as $index => $group) {
        	$id = $index+1;
        	DB::table('groups')->insert([
	        	"name" => $group["name"],
	        	"description" => $group["description"],
	        	"user_id" => $group["user_id"],
	        	"created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
	        ]);

        	foreach ($arr_groupMembers[$index] as $member) {
        		DB::table('group_members')->insert([
		        	"group_id" => $id,
		        	"user_id" => $member,
		        	"created_at" => Carbon::now()->format('Y-m-d H:i:s')
		        ]);
        	}
        	
        }
    }
}
