<?php

use Illuminate\Database\Seeder;

class JobTitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_jobTitles = [
        	"教授",
        	"博士生",
        	"碩士生",
        	"大專生"
        ];

        $i = 0;
        foreach ($arr_jobTitles as $jobTitle) {
        	$i += 1;
        	DB::table('job_titles')->insert([
	        	"name" => $jobTitle
	        ]);
        }
    }
}
