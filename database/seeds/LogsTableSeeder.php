<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_logs = [
        	[
        		"title" => "美國隊長3：英雄內戰",
        		"content" => "",
        		"start_date" => "2016-05-06",
        		"start_time" => "00:00:00",
        		"end_date" => "2016-05-06",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "奇異博士",
        		"content" => "",
        		"start_date" => "2016-11-04",
        		"start_time" => "00:00:00",
        		"end_date" => "2016-11-04",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "蜘蛛人：返校日",
        		"content" => "",
        		"start_date" => "2017-07-07",
        		"start_time" => "00:00:00",
        		"end_date" => "2017-07-07",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "雷神索爾3：諸神黃昏",
        		"content" => "",
        		"start_date" => "2017-11-03",
        		"start_time" => "00:00:00",
        		"end_date" => "2017-11-03",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "黑豹",
        		"content" => "",
        		"start_date" => "2018-02-16",
        		"start_time" => "00:00:00",
        		"end_date" => "2018-02-16",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "復仇者聯盟：無限之戰",
        		"content" => "",
        		"start_date" => "2018-04-27",
        		"start_time" => "00:00:00",
        		"end_date" => "2018-04-27",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "蟻人與黃蜂女",
        		"content" => "",
        		"start_date" => "2018-07-06",
        		"start_time" => "00:00:00",
        		"end_date" => "2018-07-06",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "驚奇隊長",
        		"content" => "",
        		"start_date" => "2019-03-08",
        		"start_time" => "00:00:00",
        		"end_date" => "2019-03-08",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "復仇者聯盟：終局之戰",
        		"content" => "",
        		"start_date" => "2019-04-26",
        		"start_time" => "00:00:00",
        		"end_date" => "2019-04-26",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "蜘蛛人：離家日",
        		"content" => "",
        		"start_date" => "2019-07-02",
        		"start_time" => "00:00:00",
        		"end_date" => "2019-07-02",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "黑寡婦",
        		"content" => "",
        		"start_date" => "2020-05-01",
        		"start_time" => "00:00:00",
        		"end_date" => "2020-05-01",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "永恆族",
        		"content" => "",
        		"start_date" => "2020-11-06",
        		"start_time" => "00:00:00",
        		"end_date" => "2020-11-06",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "上氣與十環傳奇",
        		"content" => "",
        		"start_date" => "2021-02-12",
        		"start_time" => "00:00:00",
        		"end_date" => "2021-02-12",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "奇異博士2：多元宇宙",
        		"content" => "",
        		"start_date" => "2021-05-17",
        		"start_time" => "00:00:00",
        		"end_date" => "2021-05-17",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        	[
        		"title" => "雷神索爾4：愛與雷霆",
        		"content" => "",
        		"start_date" => "2021-11-05",
        		"start_time" => "00:00:00",
        		"end_date" => "2021-11-05",
        		"end_time" => "00:00:00",
        		"color_id" => 1,
        		"user_id" => 2,
        	],
        ];

        foreach ($arr_logs as $log) {
        	DB::table('logs')->insert([
        		"title" => $log["title"],
        		"content" => $log["content"],
        		"start_date" => $log["start_date"],
        		"start_time" => $log["start_time"],
        		"end_date" => $log["end_date"],
        		"end_time" => $log["end_time"],
        		"color_id" => $log["color_id"],
        		"user_id" => $log["user_id"],
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
        	]);
        }
    }
}
