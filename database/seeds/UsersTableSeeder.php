<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_users = [
            [
                "name" => "漫威工作室",
                "student_id" => "MarvelStudios",
                "password" => Hash::make('IAmMarvelStudios'),
                "title_id" => 1,
            ],
            [
                "name" => "迪士尼",
                "student_id" => "Disney",
                "password" => Hash::make('IAmDisney'),
                "title_id" => 1,
            ],
        	[
        		"name" => "古一",
        		"student_id" => "TheAncientOne",
        		"password" => Hash::make('IAmTheAncientOne'),
        		"title_id" => 1,
        	],
        	[
        		"name" => "卡爾 莫度",
        		"student_id" => "KarlMordo",
        		"password" => Hash::make('IAmKarlMordo'),
        		"title_id" => 2,
        	],
        	[
        		"name" => "奇異博士",
        		"student_id" => "DoctorStrange",
        		"password" => Hash::make('IAmDoctorStrange'),
        		"title_id" => 3,
        	],
        	[
        		"name" => "王",
        		"student_id" => "Wong",
        		"password" => Hash::make('IAmWong'),
        		"title_id" => 4,
        	],
        	[
        		"name" => "美國隊長",
        		"student_id" => "CaptainAmerica",
        		"password" => Hash::make('IAmCaptainAmerica'),
        		"title_id" => 1,
        	],
        	[
        		"name" => "酷寒戰士",
        		"student_id" => "WinterSoldier",
        		"password" => Hash::make('IAmWinterSoldier'),
        		"title_id" => 4,
        	],
        	[
        		"name" => "緋紅女巫",
        		"student_id" => "ScarletWitch",
        		"password" => Hash::make('IAmScarletWitch'),
        		"title_id" => 4,
        	],
        	[
        		"name" => "鋼鐵人",
        		"student_id" => "IronMan",
        		"password" => Hash::make('IAmIronMan'),
        		"title_id" => 1,
        	],
        	[
        		"name" => "黑寡婦",
        		"student_id" => "BlackWidow",
        		"password" => Hash::make('IAmBlackWidow'),
        		"title_id" => 2,
        	],
        	[
        		"name" => "蜘蛛人",
        		"student_id" => "SpiderMan",
        		"password" => Hash::make('IAmSpiderMan'),
        		"title_id" => 4,
        	],
        ];

        $i = 0;
        foreach ($arr_users as $user) {
        	$i += 1;
        	DB::table('users')->insert([
	        	"name" => $user["name"],
	        	"student_id" => $user["student_id"],
	        	"password" => $user["password"],
                "title_id" => $user["title_id"],
                "permission" => 1,
	        	"created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
	        ]);
        }
    }
}
