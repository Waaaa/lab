
$(document).ready(function(){
	/* 調整表格與工作日誌事項的欄寬 */
	$(".calendar-item").width("0");
	columnHeight = $("#calendar-table-title th").width();
	$(".calendar-item").width(columnHeight);
	$("#calendar-table-title th").width(columnHeight);
	$("#calendar-table-content td").height(columnHeight);

	/* 調整工作日誌右側高度 */
	tableHeight =  $("#calendar-table").height();
	$("#calendar-item-section").height(tableHeight);
});