var HOST = window.location.hostname;
var PATH = "/";
if(HOST == "127.0.0.1"){
	var $arr_path = window.location.pathname.split("/");
	if($arr_path.includes("lab")){
		PATH = PATH + "lab/";
		if($arr_path.includes("public")){
			PATH = PATH + "public/";
		}
	}
}
var FULL_PATH = window.location.protocol+"//"+HOST+PATH;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function confirm(url){
	$.ajax({
	    url: url,
	    type: 'POST',
	    error: function(xhr) {
	    	console.log('已讀公告失敗');
	    },
	    success: function(response) {
	        console.log('已讀公告成功');
	    }
	  });
}

function currentUserId(user_id){
	$.ajax({
	    url: FULL_PATH + 'users/current',
	    type: 'POST',
	    error: function(xhr) {
	    	console.log('確認身分失敗');
	    },
	    complete: function (XMLHttpRequest, textStatus) {	    	
	    	//console.log(textStatus);
	    	if(textStatus == 'success'){
	    		console.log('確認身分成功，開始監聽個人頻道');
	    		//console.log(JSON.parse(XMLHttpRequest.responseText));
	    		user = JSON.parse(XMLHttpRequest.responseText);
	    		personalChannel(user.id);
	    	}
        
        }
	});
}
function checkUserNotice(){
	$.ajax({
	    url: FULL_PATH + 'users/notice',
	    type: 'POST',
	    error: function(xhr) {
	    	console.log('觸發個人通知失敗');
	    },
	    complete: function (XMLHttpRequest, textStatus) {	    	
	    	//console.log(textStatus);
	    	if(textStatus == 'success'){
	    		console.log('觸發個人通知成功');
	    	}
        
        }
  	});
}

//顯示
function show(announcement){
	channelMsg = ''+
	  	'<div id="announcement-'+announcement.id+'" class="modal announcement-modal" role="dialog">'+
	  		'<div class="modal-dialog" role="document">'+
	  			'<div class="modal-content">'+
					'<div class="modal-header">'+
					    '<h5 class="modal-title">'+announcement.title+'</h5>'+
					    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
					      '<span aria-hidden="true">&times;</span>'+
					    '</button>'+
					  '</div>'+
					  '<div class="modal-body">'+
					  	'<small class="text-muted mb-2">公告日期：'+announcement.published_at+'，公告人：'+announcement.publisher+'</small>'+
					    '<p>'+announcement.content+'</p>'+
					  '</div>'+
					  '<div class="modal-footer">'+
					    '<button type="button" class="btn btn-secondary" data-dismiss="modal">確定</button>'+
					  '</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	  $("#channel-msg-section").append(channelMsg);
	  $(".announcement-modal").last().modal('show');
}

function publishChannel(){
	//所有使用者
	var usersChannel = pusher.subscribe('users');
	usersChannel.bind('App\\Events\\AnnouncementPublished', function(announcement) {
		//alert(JSON.stringify(announcement));
		show(announcement);
		confirm(announcement.confirmURL);
	});
}
function personalChannel(user_id){
	//個人頻道
	var personalChannel = pusher.subscribe('users.'+user_id);
	personalChannel.bind('App\\Events\\AnnouncementPublishedForSomeone', function(announcement) {
	  	//alert(JSON.stringify(announcement));
	  	show(announcement);
		confirm(announcement.confirmURL);
	});
	checkUserNotice();
}

Pusher.logToConsole = true;
var pusher = new Pusher('189a44dd1ce747c976a1', {
  cluster: 'ap3',
  forceTLS: true
});

publishChannel();

currentUserId();
