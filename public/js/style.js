$(document).ready(function(){

	//開啟表單修改
	$(".edit-form-button").click(function(){
		event.preventDefault();
		$(".profile-input").prop("disabled", false);
		$(this).prop("disabled", true);
	});

	//讓full-page-section符合頁面高度
	fullPage();

});

function fullPage(){
	height = $(window).height();
	for(i = 0 ; i < $(".full-page-section").length ; i++){
		section = $(".full-page-section:eq("+i+")");
		position = section.position();
		padding = section.parent().css("padding-top");
		padding = padding.replace("px", "");
		//alert(padding);
		setHeight = height - position.top - (padding*4);
		//alert(setHeight);
		section.height(setHeight);
		
	}
}