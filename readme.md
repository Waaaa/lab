# 工作日誌系統
[**高科大E527實驗室工作日誌系統**](https://arcane-earth-78748.herokuapp.com/)  
本系統設計一網頁應用程式，結合資料庫系統，讓使用者可以透過瀏覽器快速的取得系統服務，以線上、即時的方式來檢視系統的公告以及新增工作日誌等資料，管理者則可透過本系統，統一管理系統所有的使用者以及進行權限管理。本系統主要分為四大功能模組，分別為：公告模組、工作日誌模組、使用者管理模組以及群組管理模組。

## 系統說明：
- 管理階層分為：最高管理者(教授) > 管理者 > 組長 > 一般使用者。
- 最高管理者與管理者以下統稱管理者。

**「使用者」模組說明**

- 系統不開放註冊帳號，只能由管理者來新增帳號，管理者可以將帳號停權(禁止登入)。
- sa不會被停權，admin不能停admin的權，只有sa可以。
- 管理者可以建立使用者帳號。
- 建立帳號只需要輸入姓名、學號、密碼、職稱即可，剩下資料可由使用者登入後自行輸入。
- 使用者密碼需經過加密。
- 管理者可以將使用者密碼重設。
- 只有sa可以重設sa的密碼，admin可以重設admin的密碼。

**「群組」模組說明**

- 管理者/組長(只有自己小組)可以新增/移除使用者。
- 最高管理者可以將使用者加進管理者群組，系統預設管理者群組為 admin。
- 最高管理者為系統預設，群組為 sa。
- sa可以管理sa群組，admin不能管理admin群組(CUD都不行)。
- 管理者可以建立群組並指派組長。
- 管理者須先建立一般使用者帳號，再新增群組並將該一般使用者升格為組長。
- 組長可以修改/刪除自己小組。
- 一般使用者不一定要有小組，也可以是個人使用者。

**「工作日誌」模組說明**

- 所有使用者都可以建立工作日誌。
- 所有使用者都可以觀看所有的工作日誌，行事曆可以在個人(包含群組)與全實驗室中切換。
- 工作日誌分：一般工作日誌(含有標題、內容)、提醒(只有標題)。
- 工作日誌可以新增小組標籤(限定自己所屬的小組)，屬於小組成員的使用者行事曆會出現該工作日誌。
- 工作日誌以顏色區分狀態類型(如重要、緊急...等)，以顏色區分。
- 組員僅能修改/刪除自己新增的工作日誌，組長可以刪除組員新增的工作日誌，管理者可以刪除所有工作日誌，但不能刪除sa群組的任何工作日誌。
- 工作日誌建立後一週即不能修改(以建立時間為準)，唯管理者可以進行刪除。

**「公告」模組說明**  
- 最高管理者與管理者可以建立及推播公告。
- 最高管理者可以CUD系統所有的公告，管理者只可以CUD自己發佈的公告。
- 推播公告功能會將公告推播給系統所有的使用者。

### 系統運行環境
* bootstrap 4.0.0
* jquery 3.3.1
* Laravel 5.4.36
* PHP 7.3.5
* Apache 2.4
* SQL server 2017

### 資料庫規劃

**logs**

| 欄位名稱       | 描述                                       |
| ---------- | ---------------------------------------- |
| id         | 資料表主鍵，唯一，自動遞增                            |
| title      | 工作日誌標題                                   |
| content    | 工作日誌內容                                   |
| start_date | 日誌開始時間 (年/月/日)                           |
| start_time | 日誌開始時間 (小時/分/秒)                          |
| end_date   | 日誌結束時間 (年/月/日)                           |
| end_time   | 日誌結束時間 (小時/分/秒)                          |
| color_id   | 外鍵，參考colors的id欄位，工作日誌的顏色與狀態類型            |
| user_id    | 外鍵，參考users的id欄位，工作日誌的建立者                 |
| group_id   | 外鍵，參考groups的id欄位，工作日誌的群組標籤，會顯示在標題 (null) |
| created_at | 建立日期                                     |
| updated_at | 最後更新日期                                   |

**log_members**

| 欄位名稱       | 描述                                                |
| ---------- | ------------------------------------------------- |
| id         | 資料表主鍵，唯一，自動遞增                                     |
| log_id     | 外鍵，參考logs的id欄位，記錄此筆參與者資料屬於哪個工作日誌                  |
| parner_id  | 外鍵，根據data_sheet來決定是參考users還是groups的id欄位，記錄此筆參與者資料 |
| data_sheet | groups或是users，紀錄該筆參與者是個人使用者還是群組                   |

**users**

| 欄位名稱       | 描述                               |
| ---------- | -------------------------------- |
| id         | 資料表主鍵，唯一，自動遞增                    |
| name       | 名稱                               |
| student_id | 學號                               |
| password   | 加密過的密碼                           |
| email      | 電子郵件 (null)                      |
| phone      | 連絡電話 (null)                      |
| title_id   | 外鍵，參考job_title的id欄位，職稱(大專生、碩士生等) |
| birth      | 生日 (null)                        |
| created_at | 建立日期                             |
| updated_at | 最後更新日期                           |
| permission | 登入權限，false為被停權                   |
| deleted_at | 刪除時間                             |

**groups**

| 欄位名稱        | 描述                    |
| ----------- | --------------------- |
| id          | 資料表主鍵，唯一，自動遞增         |
| name        | 群組名稱                  |
| description | 群組描述                  |
| user_id     | 外鍵，參考users的id欄位，群組的組長 |
| created_at  | 建立日期                  |
| updated_at  | 最後更新日期                |

**group_members**

| 欄位名稱       | 描述                   |
| ---------- | -------------------- |
| id         | 資料表主鍵，唯一，自動遞增        |
| groups_id  | 外鍵，參考groups的id欄位，小組  |
| users_id   | 外鍵，參考users的id欄位，小組成員 |
| created_at | 建立日期                 |

**colors**

| 欄位名稱  | 描述              |
| ----- | --------------- |
| id    | 資料表主鍵，唯一，自動遞增   |
| name  | 工作日誌狀態(重要，緊急…等) |
| color | 色票              |

**job_titles**

| 欄位名稱 | 描述              |
| ---- | --------------- |
| id   | 資料表主鍵，唯一，自動遞增   |
| name | 職稱(大專生，碩士生...等) |

```sql
CREATE DATABASE IF NOT EXISTS `lab` DEFAULT CHARSET='utf8' COLLATE='utf8_unicode_ci';

CREATE TABLE IF NOT EXISTS `job_titles`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_unicode_ci' COMMENT='職稱資料表';

CREATE TABLE IF NOT EXISTS `users`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	`student_id` varchar(20) NOT NULL,
	`password` varchar(255) NOT NULL,
	`email` varchar(254) DEFAULT NULL,
	`phone` varchar(20) DEFAULT NULL,
	`title_id` int(11) NOT NULL,
	`birth` date DEFAULT NULL,
	`permission` tinyint(1) DEFAULT 1,
	`created_at` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` datetime(0) DEFAULT NULL,
	`deleted_at` datetime(0) DEFAULT NULL,
	`remember_token` varchar(255) DEFAULT NULL,
	PRIMARY KEY(`id`),
	FOREIGN KEY(`title_id`) REFERENCES `job_titles`(`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_unicode_ci' COMMENT='使用者資料表';

CREATE TABLE IF NOT EXISTS `announcements`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`title` varchar(20) NOT NULL,
	`content` text,
	`top` tinyint(1) DEFAULT 0,
	`user_id` int(11) NOT NULL,
	`created_at` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` datetime(0) DEFAULT NULL,
	PRIMARY KEY(`id`),
	FOREIGN KEY(`user_id`) REFERENCES `users`(`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_unicode_ci' COMMENT='公告資料表';

CREATE TABLE IF NOT EXISTS `groups`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	`description` text,
	`user_id` int(11) NOT NULL,
	`created_at` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` datetime(0) DEFAULT NULL,
	PRIMARY KEY(`id`),
	FOREIGN KEY(`user_id`) REFERENCES `users`(`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_unicode_ci' COMMENT='群組資料表';

CREATE TABLE IF NOT EXISTS `group_members`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`group_id` int(11) NOT NULL,
	`user_id` int(11) NOT NULL,
	`created_at` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`),
	FOREIGN KEY(`group_id`) REFERENCES `groups`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY(`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_unicode_ci' COMMENT='群組成員資料表';

CREATE TABLE IF NOT EXISTS `colors`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	`color` varchar(7) NOT NULL,
	PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_unicode_ci' COMMENT='顏色狀態資料表';

CREATE TABLE IF NOT EXISTS `logs`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`title` varchar(20) NOT NULL,
	`content` text,
	`start_date` date NOT NULL,
	`start_time` time NOT NULL,
	`end_date` date NOT NULL,
	`end_time` time NOT NULL,
	`color_id` int(11) NOT NULL,
	`user_id` int(11) NOT NULL,
	`group_id` int(11) NOT NULL,
	`created_at` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` datetime(0) DEFAULT NULL,
	PRIMARY KEY(`id`),
	FOREIGN KEY(`color_id`) REFERENCES `colors`(`id`) ON UPDATE CASCADE,
	FOREIGN KEY(`group_id`) REFERENCES `groups`(`id`) ON UPDATE CASCADE,
	FOREIGN KEY(`user_id`) REFERENCES `users`(`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_unicode_ci' COMMENT='工作日誌資料表';

CREATE TABLE IF NOT EXISTS `notices`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`announcement_id` int(11) NOT NULL,
	`created_at` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` datetime(0) DEFAULT NULL,
	PRIMARY KEY(`id`),
	FOREIGN KEY(`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY(`announcement_id`) REFERENCES `announcements`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_unicode_ci' COMMENT='通知資料表';
```

### 系統預設資料
```php
$ php artisan migrate --seed
```

```php
//職稱預設資料
[
	"教授",
	"博士生",
	"碩士生",
	"大專生"
];

//使用者預設資料
[
    [
        "name" => "漫威工作室",
        "student_id" => "MarvelStudios",
        "password" => Hash::make('IAmMarvelStudios'),
        "title_id" => 1,
    ],
    [
        "name" => "迪士尼",
        "student_id" => "Disney",
        "password" => Hash::make('IAmDisney'),
        "title_id" => 1,
    ],
	[
		"name" => "古一",
		"student_id" => "TheAncientOne",
		"password" => Hash::make('IAmTheAncientOne'),
		"title_id" => 1,
	],
	[
		"name" => "卡爾 莫度",
		"student_id" => "KarlMordo",
		"password" => Hash::make('IAmKarlMordo'),
		"title_id" => 2,
	],
	[
		"name" => "奇異博士",
		"student_id" => "DoctorStrange",
		"password" => Hash::make('IAmDoctorStrange'),
		"title_id" => 3,
	],
	[
		"name" => "王",
		"student_id" => "Wong",
		"password" => Hash::make('IAmWong'),
		"title_id" => 4,
	],
	[
		"name" => "美國隊長",
		"student_id" => "CaptainAmerica",
		"password" => Hash::make('IAmCaptainAmerica'),
		"title_id" => 1,
	],
	[
		"name" => "酷寒戰士",
		"student_id" => "WinterSoldier",
		"password" => Hash::make('IAmWinterSoldier'),
		"title_id" => 4,
	],
	[
		"name" => "緋紅女巫",
		"student_id" => "ScarletWitch",
		"password" => Hash::make('IAmScarletWitch'),
		"title_id" => 4,
	],
	[
		"name" => "鋼鐵人",
		"student_id" => "IronMan",
		"password" => Hash::make('IAmIronMan'),
		"title_id" => 1,
	],
	[
		"name" => "黑寡婦",
		"student_id" => "BlackWidow",
		"password" => Hash::make('IAmBlackWidow'),
		"title_id" => 2,
	],
	[
		"name" => "蜘蛛人",
		"student_id" => "SpiderMan",
		"password" => Hash::make('IAmSpiderMan'),
		"title_id" => 4,
	],
];

//群組預設資料
[
	[
		"name" => "sa",
		"description" => "最高管理者群組。",
		"user_id" => 1
	],
	[
		"name" => "admin",
		"description" => "管理者群組。",
		"user_id" => 2
	],
	[
		"name" => "卡瑪泰姬",
		"description" => "位於尼泊爾加德滿都的魔法聖殿。",
		"user_id" => 3
	],
	[
		"name" => "反對派陣營",
		"description" => "我反對R!",
		"user_id" => 7
	],
	[
		"name" => "贊成派陣營",
		"description" => "我贊成R!",
		"user_id" => 10
	],
];

//群組成員預設資料
[
	[1],
	[2],
	[3, 4, 5, 6],
	[7, 8, 9],
	[10, 11, 12],
];

//公告預設資料
[
	[
		"title" => "安心出門，安心回家之奇異博士行車小心。",
		"content" => "《奇異博士》（英語：Doctor Strange）是一部2016年美國超級英雄電影，改編自漫威漫畫旗下的同名漫畫人物。本片為漫威影業製作、華特迪士尼工作室電影發行，漫威電影宇宙的第十四部電影。由史考特·德瑞森執導，德瑞森、強·史派茲與C·羅柏·卡吉爾負責編劇。電影主演包括班奈狄克·康柏拜區、蒂妲·史雲頓、奇維托·艾吉佛、邁茲·米克森、黃凱旋、麥可·斯圖巴、班傑明·布萊特、史考特·艾金斯和瑞秋·麥亞當斯。",
		"user_id" => 1
	],
	[
		"title" => "大家要打起來囉!",
		"content" => "在這部電影中，美國隊長和鋼鐵人因一項管理異能者活動的法案而為此態度對立，進而各自組建了陣營來對抗彼此。",
		"user_id" => 1
	],
];

//通知預設資料
[
	[3, 4, 5, 6],
	[7, 8, 9, 10, 11, 12],
];

//顏色狀態預設資料
[
	[
		"name" => "普通",
		"color" => "#4B515D"
	],
	[
		"name" => "重要",
		"color" => "#FF8800"
	],
	[
		"name" => "緊急",
		"color" => "#CC0000"
	],
];

//工作日誌預設資料
[
	[
		"title" => "美國隊長3：英雄內戰",
		"content" => "",
		"start_date" => "2016-05-06",
		"start_time" => "00:00:00",
		"end_date" => "2016-05-06",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "奇異博士",
		"content" => "",
		"start_date" => "2016-11-04",
		"start_time" => "00:00:00",
		"end_date" => "2016-11-04",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "蜘蛛人：返校日",
		"content" => "",
		"start_date" => "2017-07-07",
		"start_time" => "00:00:00",
		"end_date" => "2017-07-07",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "雷神索爾3：諸神黃昏",
		"content" => "",
		"start_date" => "2017-11-03",
		"start_time" => "00:00:00",
		"end_date" => "2017-11-03",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "黑豹",
		"content" => "",
		"start_date" => "2018-02-16",
		"start_time" => "00:00:00",
		"end_date" => "2018-02-16",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "復仇者聯盟：無限之戰",
		"content" => "",
		"start_date" => "2018-04-27",
		"start_time" => "00:00:00",
		"end_date" => "2018-04-27",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "蟻人與黃蜂女",
		"content" => "",
		"start_date" => "2018-07-06",
		"start_time" => "00:00:00",
		"end_date" => "2018-07-06",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "驚奇隊長",
		"content" => "",
		"start_date" => "2019-03-08",
		"start_time" => "00:00:00",
		"end_date" => "2019-03-08",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "復仇者聯盟：終局之戰",
		"content" => "",
		"start_date" => "2019-04-26",
		"start_time" => "00:00:00",
		"end_date" => "2019-04-26",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "蜘蛛人：離家日",
		"content" => "",
		"start_date" => "2019-07-02",
		"start_time" => "00:00:00",
		"end_date" => "2019-07-02",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "黑寡婦",
		"content" => "",
		"start_date" => "2020-05-01",
		"start_time" => "00:00:00",
		"end_date" => "2020-05-01",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "永恆族",
		"content" => "",
		"start_date" => "2020-11-06",
		"start_time" => "00:00:00",
		"end_date" => "2020-11-06",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "上氣與十環傳奇",
		"content" => "",
		"start_date" => "2021-02-12",
		"start_time" => "00:00:00",
		"end_date" => "2021-02-12",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "奇異博士2：多元宇宙",
		"content" => "",
		"start_date" => "2021-05-17",
		"start_time" => "00:00:00",
		"end_date" => "2021-05-17",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
	[
		"title" => "雷神索爾4：愛與雷霆",
		"content" => "",
		"start_date" => "2021-11-05",
		"start_time" => "00:00:00",
		"end_date" => "2021-11-05",
		"end_time" => "00:00:00",
		"color_id" => 1,
		"user_id" => 2,
	],
];
```

### 系統初始化作業流程
1. 新增sa群組、admin群組，新增最高管理者與管理者帳號
3. 新增預設使用者

