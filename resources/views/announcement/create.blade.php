@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/calendar.css') }}">
@endsection
@section('content')
<div id="calendar-section">
	<div class="row">
		<div id="calendar-breadcrumb-section" class="col-12">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb bg-light">
			    <li class="breadcrumb-item">公告列表</li>
			    <li class="breadcrumb-item active" aria-current="page">新增公告</li>
			  </ol>
			</nav>
		</div>
	</div>
	<div id="calendar-content-section">
		<div class="col-12 content-section">
			<div class="row">
				<div class="form-hint col-12 col-xl-3">
					<h4><b>新增公告</b></h4>
					<p>所有使用者都可以觀看您建立的公告</p>
				</div>
				<form method="POST" class="col-12 col-xl-8" action="{{route('announcement.store')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group col-12">
					  <label>公告標題 <a class="text-danger">*</a></label>
					  <input type="text" name="title" class="form-control" placeholder="標題" value="{{old('title')}}" required>
					  @if($errors->has('title'))
					  	<small class="form-text text-danger">{{$errors->first('title')}}</small>
					  @else
					  	<small class="form-text text-muted">標題最多50個字</small>	
					  @endif
					</div>	

					<hr>

					
					
					<div class="form-group col-12">
					  <label>公告內容</label>
					  <textarea name="content" class="form-control" rows="10">{{{old('content')}}}</textarea>
					  @if($errors->has('content'))
					  	<small class="form-text text-danger">{{$errors->first('content')}}</small>
					  @endif
					</div>

					<hr>
					<div class="form-group col-12">
						<div class="form-check">
						  <input class="form-check-input" type="checkbox" value="1" name="top" id="top">
						  <label class="form-check-label" for="top">
						    公告置頂
						  </label>
						</div>
					</div>	
					<div class="form-group col-12 mb-4">
						<div class="form-check">
						  <input class="form-check-input" type="checkbox" value="1" name="broadcast" id="broadcast">
						  <label class="form-check-label" for="broadcast">
						    推播給系統所有的使用者
						  </label>
						</div>
					</div>	
					<div class="col-12 mt-5 text-right">
						<button type="submit" class="btn btn-info" >送出</button>
						<a href="{{route('announcement.index')}}" class="btn btn-danger" onclick="return confirm('確定要取消這次的新增嗎? 系統將不會儲存您的資料')">取消</a>
					</div>
				</form>	
			</div>	
		</div>		
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/calendar.js') }}"></script>
@endsection
