@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/group.css') }}">
@endsection
@section('content')
<div id="group-section">
	<div id="group-head-section" class="row">
		<div id="group-breadcrumb-section" class="col-12">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb bg-light">
			  	<li class="breadcrumb-item active" aria-current="page">公告列表</li>
			  </ol>
			</nav>
		</div>
	</div>
	<div id="group-content-section">
		@if (Session::has('successMessage'))
			<div class="col-12 alert alert-success" role="alert">
			  {{ Session::get('successMessage') }}
			</div>
		@endif
		@if (Session::has('errorMessage'))
			<div class="col-12 alert alert-danger" role="alert">
			  {{ Session::get('errorMessage') }}
			</div>
		@endif

		<table id="group-table" class="table">
			<thead>
				<tr id="group-table-title">
				  <th scope="col" class="w-50">標題</th>
				  <th scope="col">公告日期</th>
				  <th scope="col">公告人</th>
				  @if( Auth::user()->isAdmin() || Auth::user()->isSuperAdmin()  )
				  	<th scope="col"></th>
				  @endif
				</tr>
			</thead>
			<tbody id="group-table-content">
				@foreach($announcements as $announcement)
				<tr>
					<td>
						@if($announcement->top == 1)
							<span class="badge badge-info top-tag">置頂</span>&nbsp;
						@endif
						<a href="{{route('announcement.show', ['announcement_id' => $announcement->id])}}">{{$announcement->title}}</a>
					</td>
					<td>{{date('Y-m-d H:i', strtotime($announcement->created_at))}}</td>
					<td>{{$announcement->ownerUser->name}}</td>
					@if( Auth::user()->isAdmin() || Auth::user()->isSuperAdmin()  )
					<td>
						<div class="dropdown">
						  <button class="btn btn-info dropdown-toggle" type="button" id="groupMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  	<i class="fas fa-bars"></i>
						  </button>
						  <div class="dropdown-menu" aria-labelledby="groupMenuButton">
						  		<a class="dropdown-item" href="{{route('announcement.notice', ['announcement_id' => $announcement->id])}}" onclick="return confirm('確定要推播這則的公告嗎?')">推播</a>
							    <a class="dropdown-item" href="{{route('announcement.edit', ['announcement_id' => $announcement->id])}}">修改</a>
							    <form action="{{route('announcement.destroy', ['announcement_id' => $announcement->id])}}" method="POST">
							    	{{ csrf_field() }}
									{{ method_field('DELETE') }}
							    	<button class="dropdown-item text-danger" type="submit" onclick="return confirm('確定要刪除嗎? 系統將無法復原您的動作')">刪除</button>
							    </form>
						    
						  </div>
						</div>
					</td>
					@endif
				</tr>
				@endforeach
			</tbody>
		</table>

		<div class="col-12 pr-0">
	    @if($announcements->lastPage() > 1)
	      <nav>
	        <ul class="pagination justify-content-end">
	          @if( $announcements->currentPage() != 1 )
	            <li class="page-item"><a class="page-link" href="{{ $announcements->url($announcements->currentPage()-1) }}">Previous</a></li>
	          @endif
	          @for($i = 1 ; $i <= $announcements->lastPage(); $i++)

	            <li class="page-item {{ ($announcements->currentPage() == $i) ? 'active' : '' }}"><a class="page-link" href="{{ $announcements->url($i) }}">{{ $i }}</a></li>
	          @endfor
	          @if( $announcements->currentPage() != $announcements->lastPage() )
	            <li class="page-item"><a class="page-link" href="{{ $announcements->nextPageUrl() }}">Next</a></li>
	          @endif
	        </ul>
	      </nav>
	    @endif
	  </div>
	</div>
</div>
@if(Auth::user()->isSuperAdmin() || Auth::user()->isAdmin())
	<a href="{{route('announcement.create')}}" class="btn btn-danger float-insert-button"><i class="fas fa-plus"></i></a>
@endif	
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
@endsection
