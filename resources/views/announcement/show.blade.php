@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/calendar.css') }}">
@endsection
@section('content')
<div id="calendar-section">
	<div class="row">
		<div id="calendar-breadcrumb-section" class="col-12">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb bg-light">
			    <li class="breadcrumb-item">公告列表</li>
			    <li class="breadcrumb-item active" aria-current="page">{{$announcement->title}}</li>
			  </ol>
			</nav>
		</div>
	</div>
	<div id="calendar-content-section">
		<div class="col-12 content-section">
			<small><a href="{{route('announcement.index')}}" class="text-muted"><i class="fas fa-chevron-left"></i>&nbsp;&nbsp;返回</a></small>	
			<br>
			<br>
			<h3 class="content-title">
				<b>{{ $announcement->title }}</b>
			</h3>
			<small class="pt-2 text-muted">公告日期：{{date('Y-m-d H:i', strtotime($announcement->created_at))}}，最後更新日期：{{date('Y-m-d H:i', strtotime($announcement->updated_at))}}，公告人：{{$announcement->ownerUser->name}}</small>
			<hr>
			{{ $announcement->content }}
		</div>	
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/calendar.js') }}"></script>
@endsection
