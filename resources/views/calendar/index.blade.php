@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/calendar.css') }}">
@endsection
@section('content')
<div class="row" id="calendar-section">
	<div id="calendar-title-section" class="col-12 col-xl-8">	
		<div class="row">
			<div id="calendar-title" class="col-6">
				{{$year}} 年 {{$month}} 月 {{sprintf("%02d", $day)}} 日
				<br>
				<small class="pt-2">{{$calendarTitle}}</small>
			</div>
			<div class="col-6 text-right calendar-date-button">
				<a href="{{$preLink}}"><i class="fas fa-chevron-left"></i></a>
				&emsp;&emsp;
				<a href="{{$nextLink}}"><i class="fas fa-chevron-right"></i></a>
			</div>	
		</div>	
	</div>	
	<div class="col-12 col-xl-8">		
		
		<div id="calendar-table-section">
			<table id="calendar-table" class="table">
				<thead>
					<tr id="calendar-table-title">
					  <th scope="col">日</th>
					  <th scope="col">一</th>
					  <th scope="col">二</th>
					  <th scope="col">三</th>
					  <th scope="col">四</th>
					  <th scope="col">五</th>
					  <th scope="col">六</th>
					</tr>
				</thead>
				<tbody id="calendar-table-content">
					@for($i = 1-$week; $i <= $days;)
						<tr>
						@for($j = 0; $j < 7; $j++)
							@if($i > $days || $i < 1)
								<td>&nbsp;</td>
							@else
								@php( $logs = $tmp_log->getLogsByDate("$year-$month-$i", $type) )
								@if($i == $day)
									<td><div id='current-day'>{{$i}}</div>
								@else
									<td><a href="{{route('calendar.month.log.list', ['type' => $type, 'year' => $year, 'month' => $month, 'day' => $i])}}">{{$i}}</a>
								@endif
								
								
								@php( $limit = (count($logs) > 2) ? 2 : count($logs))
								@for($x = 0; $x < $limit ; $x++)
									<div class='calendar-item'>
										<a href="{{route('log.show', ['log_id' => $logs[$x]->id ])}}"><i style='color: {{$logs[$x]->color->color}};' class='fas fa-sticky-note'></i> {{$logs[$x]->fullTitle}}</a>
									</div>
								@endfor
								@if($logs->count() > 2)
									<p class="text-right">還有 {{$logs->count()-2}} 項</p>
								@endif
								
								</td>
							@endif
							@php( $i++ )
						@endfor
						</tr>
					@endfor
					<!--
						<div class="calendar-item">
							<i class="fas fa-sticky-note"></i> 107 專題小組 1
						</div>
					-->
					
				</tbody>
			</table>
		</div>
	</div>
	<div id="calendar-item-section" class="col-12 col-xl-4">
		@if (Session::has('successMessage'))
			<div class="col-12 alert alert-success" role="alert">
			  {{ Session::get('successMessage') }}
			</div>
		@endif
		@if (Session::has('errorMessage'))
			<div class="col-12 alert alert-danger" role="alert">
			  {{ Session::get('errorMessage') }}
			</div>
		@endif
		@yield('calendar-item-section')
	</div>
</div>


<a href="{{route('log.create')}}" class="btn btn-danger float-insert-button"><i class="fas fa-plus"></i></a>
<a title="切換個人/全實驗室工作日誌" href="{{$switchTypeLink}}" class="btn btn-info float-change-button">{!! $icon !!}</a>


@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/calendar.js') }}"></script>
@endsection