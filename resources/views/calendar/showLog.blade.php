@extends('calendar.index')
@section('calendar-item-section')
<div id="calendar-item-detail" class="content-section">
	<div class="mb-4 row">
		<div class="col-6">
			<small><a href="{{route('calendar.month.log.list', ['year' => $year, 'month' => $month, 'day' => $day])}}" class="text-muted"><i class="fas fa-chevron-left"></i>&nbsp;&nbsp;返回</a></small>	
		</div>
		<div class="col-6 text-right">
			
			@if( ($log->user_id == Auth::user()->id) )
				<a href="#" class="text-muted"><i class="far fa-edit"></i></a>
				&nbsp;&nbsp;
				<a href="#" onclick="return confirm('確定要刪除嗎?  系統將無法復原您的動作')" class="text-muted"><i class="far fa-trash-alt"></i></a>
			@elseif(Auth::user()->id == @$log->ownerGroup->user_id || (!Auth::user()->isGuest()) )
				<a href="#" onclick="return confirm('確定要刪除嗎?  系統將無法復原您的動作')" class="text-muted"><i class="far fa-trash-alt"></i></a>
			@endif
		</div>
	</div>
	<h5><b><i style='color: {{$log->color->color}};' class='fas fa-sticky-note'></i>&nbsp;&nbsp;{{$log->title}}</b></h5>
	<small class="text-muted">{{$log->start_date}} {{$log->start_time}} 到 {{$log->end_date}} {{$log->end_time}}</small>	
	<br>
	<small><i class="fas fa-user"></i>&nbsp;&nbsp;{{$log->ownerUser->name}}</small>
	@if($log->ownerGroup !== NULL)
		<br>
		<small><i class="fas fa-users"></i>&nbsp;&nbsp;{{$log->ownerGroup->name}}</small>
	@endif
	<hr>
	<div class="text-align-right-and-left">{!! $content !!}</div>
</div>
@endsection