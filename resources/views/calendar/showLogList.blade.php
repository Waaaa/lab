@extends('calendar.index')
@section('calendar-item-section')
@foreach ($logs as $log)
	<div class="content-section mb-2">
		<h5><b><i style='color: {{$log->color->color}};' class='fas fa-sticky-note'></i>&nbsp;&nbsp;<a href="{{route('log.show', ['log_id' => $log->id ])}}" class="text-dark">{{$log->title}}</a></b></h5>
		<small class="text-muted">{{$log->start_date}} {{date('H:i',strtotime($log->start_time))}} 到 {{$log->end_date}} {{date('H:i',strtotime($log->end_time))}}</small>	
		<br>
		<small><i class="fas fa-user"></i> {{$log->ownerUser->name}}</small>
		@if($log->ownerGroup !== NULL)
			<br>
			<small><i class="fas fa-users"></i>&nbsp;&nbsp;{{$log->ownerGroup->name}}</small>
		@endif

		@if( ($log->user_id == Auth::user()->id) )
			<hr>
			<div class="row">
				<div class="col-6" style="font-size: 0.8rem;">
					建立於 {{date('Y-m-d H:i',strtotime($log->created_at))}} 
				</div>
				<div class="col-6 text-right">
					<form action="{{route('log.destroy', ['log_id' => $log->id])}}" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<a href="{{route('log.edit', ['log_id' => $log->id])}}" class="btn btn-outline-warning"><i class="far fa-edit"></i></a>
						<button onclick="return confirm('確定要刪除嗎?  系統將無法復原您的動作')" class="btn btn-outline-danger"><i class="far fa-trash-alt"></i></button>
					</form>
				</div>
			</div>
		@elseif(Auth::user()->id == @$log->ownerGroup->user_id || (!Auth::user()->isGuest()) )
			<hr>
			<div class="row">
				<div class="col-6" style="font-size: 0.8rem;">
					建立於 {{date('Y-m-d H:i',strtotime($log->created_at))}} 
				</div>
				<div class="col-6 text-right">
					<form action="{{route('log.destroy', ['log_id' => $log->id])}}" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button onclick="return confirm('確定要刪除嗎?  系統將無法復原您的動作')" class="btn btn-outline-danger"><i class="far fa-trash-alt"></i></button>
					</form>
				</div>
			</div>	
		@endif
	</div>
@endforeach
@endsection