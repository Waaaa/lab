@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/group.css') }}">
@endsection
@section('content')
<div id="group-section">
	<div id="group-head-section" class="row">
		<div id="group-breadcrumb-section" class="col-12">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb bg-light">
			  	<li class="breadcrumb-item">群組管理</li>
			    <li class="breadcrumb-item">群組列表</li>
			    <li class="breadcrumb-item active" aria-current="page">新增群組</li>
			  </ol>
			</nav>
		</div>
	</div>
	<div id="group-content-section">
		<div class="col-12 content-section">
			<div class="row">
				<div class="form-hint col-12 col-xl-3">
					<h4><b>新增群組</b></h4>
					<p>群組描述為選填，組員不須勾選組長</p>
				</div>
				<form method="POST" class="col-12 col-xl-6 offset-xl-1" action="{{route('group.store')}}">				
					{{ csrf_field() }}
					<div class="form-group col-12">
					  <label>群組名稱 <a class="text-danger">*</a></label>
					  <input type="text" name="name" class="form-control" placeholder="群組名稱" value="{{old('name')}}" required>
					  @if($errors->has('name'))
					  	<small class="form-text text-danger">{{ $errors->first('name') }}</small>
					  @else
					  	<small class="form-text text-muted">群組名稱最多50個字</small>
					  @endif
					</div>
					<div class="form-group col-12">
					  <label>描述</label>
					  <textarea name="description" class="form-control" rows="5">{{{old('description')}}}</textarea>
					  @if($errors->has('description'))
					  	<small class="form-text text-danger">{{$errors->first('description')}}</small>
					  @endif
					</div>

					<div class="form-group col-12">
					  <label>組長 <a class="text-danger">*</a></label>
					  <select name="leader_id" class="form-control" required>
					  	@foreach($users as $user)
					  		<option value="{{$user->id}}" {{ (old('leader_id') == $user->id)? 'selected' : '' }}>{{$user->name}}</option>
					  	@endforeach
					  </select>
					  @if($errors->has('leader_id'))
					  	<small class="form-text text-danger">{{$errors->first('leader_id')}}</small>
					  @endif 
					</div>
					<hr>
					<div class="form-group col-12">
						<label>組員</label>
						@foreach($users as $user)
							<div class="form-check">
							  <input class="form-check-input" type="checkbox" value="{{$user->id}}" name="members[]">
							  <label class="form-check-label">
							    {{$user->name}} <small>({{$user->student_id}})</small>
							  </label>
							</div>
						@endforeach
					</div>
					<div class="col-12 mt-5 text-right">
						<button type="submit"  class="btn btn-info">送出</button>
						<a href="{{route('group.index')}}" class="btn btn-danger" onclick="return confirm('確定要取消這次的新增嗎? 系統將不會儲存您的資料')">取消</a>
					</div>
				</form>	
			</div>	
		</div>
	</div>
</div>

@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
@endsection
