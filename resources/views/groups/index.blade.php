@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/group.css') }}">
@endsection
@section('content')
<div id="group-section">
	<div id="group-head-section" class="row">
		<div id="group-breadcrumb-section" class="col-8">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb bg-light">
			  	<li class="breadcrumb-item active" aria-current="page">群組管理</li>
			    <li class="breadcrumb-item active" aria-current="page">群組列表</li>
			  </ol>
			</nav>
		</div>
		<div id="group-setting-section" class="col-4">	
			<div class="btn-group">
			  <a href="{{route('group.personal')}}" class="btn {{ ($type == 'personal') ? 'btn-info' : 'btn-secondary'  }}"><i class="fas fa-list"></i>&emsp;個人</a>
			  <a href="{{route('group.index')}}" class="btn {{ ($type != 'personal') ? 'btn-info' : 'btn-secondary'  }}"><i class="fas fa-list"></i>&emsp;實驗室</a>
			</div>
		</div>
	</div>
	<div id="group-content-section">
		@if (Session::has('successMessage'))
			<div class="col-12 alert alert-success" role="alert">
			  {{ Session::get('successMessage') }}
			</div>
		@endif
		@if (Session::has('errorMessage'))
			<div class="col-12 alert alert-danger" role="alert">
			  {{ Session::get('errorMessage') }}
			</div>
		@endif

		<table id="group-table" class="table">
			<thead>
				<tr id="group-table-title">
				  <th scope="col">小組名稱</th>
				  <th scope="col">描述</th>
				  <th scope="col">組長</th>
				  <th scope="col">小組人數</th>
				  <th scope="col">建立於</th>
				  <th scope="col"></th>
				</tr>
			</thead>
			<tbody id="group-table-content">
				@foreach($groups as $group)
				<tr>
					<td>{{$group->name}}</td>
					<td>{{$group->description}}</td>
					<td>{{$group->leader->name}}</td>
					<td>共 {{$group->members->count()}} 人</td>
					<td>{{date('Y-m-d H:i', strtotime($group->created_at))}}</td>
					<td>
						<div class="dropdown">
						  <button class="btn btn-info dropdown-toggle" type="button" id="groupMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  	<i class="fas fa-bars"></i>
						  </button>
						  <div class="dropdown-menu" aria-labelledby="groupMenuButton">
						  	<a class="dropdown-item text-info" href="{{route('group.show', ['group_id' => $group->id])}}">詳細資訊</a>
						  	@if( ((Auth::user()->isAdmin() || Auth::user()->isSuperAdmin() ) || (Auth::user()->id == $group->user_id)) )
							  	<div class="dropdown-divider"></div>
							    <a class="dropdown-item" href="{{route('group.edit', ['group_id' => $group->id])}}">修改</a>
							    @if($group->isGuestGroup())
							    <form action="{{ route('group.destroy', ['group_id' => $group->id]) }}" method="POST">
							    	{{ csrf_field() }}
									{{ method_field('DELETE') }}
							    	<button class="dropdown-item text-danger" type="submit" onclick="return confirm('確定要刪除嗎? 系統將無法復原您的動作')">刪除</button>
							    </form>
							    @endif()
						    @endif
						  </div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		<div class="col-12 pr-0">
	    @if($groups->lastPage() > 1)
	      <nav>
	        <ul class="pagination justify-content-end">
	          @if( $groups->currentPage() != 1 )
	            <li class="page-item"><a class="page-link" href="{{ $groups->url($groups->currentPage()-1) }}">Previous</a></li>
	          @endif
	          @for($i = 1 ; $i <= $groups->lastPage(); $i++)

	            <li class="page-item {{ ($groups->currentPage() == $i) ? 'active' : '' }}"><a class="page-link" href="{{ $groups->url($i) }}">{{ $i }}</a></li>
	          @endfor
	          @if( $groups->currentPage() != $groups->lastPage() )
	            <li class="page-item"><a class="page-link" href="{{ $groups->nextPageUrl() }}">Next</a></li>
	          @endif
	        </ul>
	      </nav>
	    @endif
	  	</div>
	</div>
</div>
<a href="{{route('group.create')}}" class="btn btn-danger float-insert-button"><i class="fas fa-plus"></i></a>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
@endsection
