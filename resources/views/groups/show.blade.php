@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/group.css') }}">
@endsection
@section('content')
<div id="group-section">
	<div id="group-head-section" class="row">
		<div id="group-breadcrumb-section" class="col-12">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb bg-light">
			  	<li class="breadcrumb-item">群組管理</li>
			    <li class="breadcrumb-item">群組列表</li>
			    <li class="breadcrumb-item active" aria-current="page">詳細資訊</li>
			    <li class="breadcrumb-item active" aria-current="page">{{$group->name}}</li>
			  </ol>
			</nav>
		</div>
	</div>
	<div id="group-content-section" class="mb-2">
		<div class="col-12 content-section">
			<div class="row">
				<div class="form-hint col-12 col-xl-3">
					<h4><b>詳細資訊</b></h4>
					<p>關於 {{$group->name}} 群組</p>
				</div>
				<div class="col-12 col-xl-6 offset-xl-1">
					<div class="form-group col-12">
					  <label>群組名稱</label>
					  <input type="text" name="name" class="form-control" value="{{$group->name}}" disabled="true">
					</div>
					<div class="form-group col-12">
					  <label>描述</label>
					  <input type="text" name="description" class="form-control" value="{{$group->description}}"  disabled="true">
					</div>

					<div class="form-group col-12">
					  <label>組長</label>
					  <input type="text" name="leader" class="form-control" value="{{$group->leader->name}}"  disabled="true">
					</div>
					<hr>
					<div class="form-group col-12">
						<label>組員列表 (共 {{$group->members->count()}} 人)</label>

						@foreach($group->members as $member)
							<br>#{{$loop->iteration}}&emsp;{{$member->user->name}} <small>({{$member->user->student_id}})</small>
						@endforeach						
					</div>
					<div class="col-12 mt-5 text-right">
						<a href="{{route('group.index')}}" class="btn btn-info">返回</a>
					</div>
				</div>
			</div>		
		</div>
	</div>

	@foreach($logs as $log)
		<div class="content-section mb-2">
			<h5><b><i style='color: {{$log->color->color}};' class='fas fa-sticky-note'></i>&nbsp;&nbsp;<a href="{{route('log.show', ['log_id' => $log->id ])}}" class="text-dark">{{$log->title}}</a></b></h5>
			<small class="text-muted">{{$log->start_date}} {{date('H:i',strtotime($log->start_time))}} 到 {{$log->end_date}} {{date('H:i',strtotime($log->end_time))}}</small>	
			<br>
			<small><i class="fas fa-user"></i> {{$log->ownerUser->name}}</small>
			@if($log->ownerGroup !== NULL)
				<br>
				<small><i class="fas fa-users"></i>&nbsp;&nbsp;{{$log->ownerGroup->name}}</small>
			@endif

			<hr>
			<div class="row">
				<div class="col-6" style="font-size: 0.8rem;">
					建立於 {{date('Y-m-d H:i',strtotime($log->created_at))}} 
				</div>
			</div>	

		</div>
	@endforeach
	<div class="col-12 pr-0">
    @if($logs->lastPage() > 1)
      <nav>
        <ul class="pagination justify-content-end">
          @if( $logs->currentPage() != 1 )
            <li class="page-item"><a class="page-link" href="{{ $logs->url($logs->currentPage()-1) }}">Previous</a></li>
          @endif
          @for($i = 1 ; $i <= $logs->lastPage(); $i++)

            <li class="page-item {{ ($logs->currentPage() == $i) ? 'active' : '' }}"><a class="page-link" href="{{ $logs->url($i) }}">{{ $i }}</a></li>
          @endfor
          @if( $logs->currentPage() != $logs->lastPage() )
            <li class="page-item"><a class="page-link" href="{{ $logs->nextPageUrl() }}">Next</a></li>
          @endif
        </ul>
      </nav>
    @endif
  	</div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
@endsection
