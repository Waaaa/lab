<html>
<head>
    <title>E527 工作日誌系統</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
</head>
<body>
<div id="loginPage" class="jumbotron jumbotron-fluid mb-0">
  <div class="container">
    <center><h2 class="font-weight-bold mb-4">E527 工作日誌系統</h2></center>
    <div  id="loginBox" class="col-md-8 offset-md-2 col-lg-6 offset-lg-3 col-12 offset-0 align-middle text-center">
        @yield('msg', '歡迎來到E527 工作日誌系統')
        <br><br>
        <small><a href="{{ route('home') }}">返回首頁</a></small>
    </div>
  </div>
</div>

</body>
</html>