<!DOCTYPE html>
<html>
<head>
	<title>E527 工作日誌系統</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Roboto:400,700,900i" rel="stylesheet">

	<!-- fontawesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
	@yield('css')
</head>
<body>
	<section id="page-section" class="container-fluid">
		<div class="row">
			<div id="sidebar-section" class="col-1 col-xl-2">
				@include('layouts.sidebar')
			</div>
			<div id="main-section" class="col-11 col-xl-10">
				@yield('content')
			</div>
		</div>
	</section>
	<section id="channel-msg-section">
		
	</section>
</body>
<!-- bootstrap -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- pusher -->
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script src="{{ asset('js/pusher.js') }}"></script>

@yield('js')

</html>