<div id="user-field-section" class="row">
	<!-- 54*54 
	<div id="user-field-image-section"><img width="54" height="54" src="{{ asset('image/user.png') }}"></div>
	-->
	<div id="user-field-image-section">{{mb_substr( Auth::user()->name, 0, 1,"utf-8")}}</div>
	<div id="user-field-text-section" class="d-none d-xl-block"><h4>{{ Auth::user()->name }}</h4><small>{{ Auth::user()->jobTitle->name }}</small></div>
</div>
<div id="menu-section" class="row">
	<ul id="main-menu-section" class="nav flex-column">
		<li class="nav-item">
		    <a class="nav-link" href="{{ route('announcement.index') }}">
		    	<font class="d-block d-xl-none"><i class="fas fa-info-circle"></i></font>
		    	<font class="d-none d-xl-block">公告列表</font>
		    </a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="{{route('calendar.month.log.list', ['type' => 'lab', 'year' => date('Y'), 'month' => date('m'), 'day' => date('d')])}}">
		    	<font class="d-block d-xl-none"><i class="fas fa-calendar-alt"></i></font>
		    	<font class="d-none d-xl-block">工作日誌</font>
		    </a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="{{route('user.index')}}">
		    	<font class="d-block d-xl-none"><i class="fas fa-user-cog"></i></font>
		    	<font class="d-none d-xl-block">使用者列表</font>
		    </a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="{{route('group.index')}}">
		    	<font class="d-block d-xl-none"><i class="fas fa-users-cog"></i></font>
		    	<font class="d-none d-xl-block">群組列表</font>
		    </a>
		  </li>
		  
	</ul>
	<div id="bottom-menu-section" class="col-1 col-xl-2 fixed-bottom">
		<ul id="second-menu-section" class="nav flex-column">
		  <li class="nav-item">
		    <a class="nav-link" href="{{ route('logout') }}" onclick="return confirm('確定要登出嗎?')">
		    	<font class="d-block d-xl-none"><i class="fas fa-sign-out-alt"></i></font>
		    	<font class="d-none d-xl-block">登出</font>
		    </a>
		  </li>
		</ul>
	</div>
</div>