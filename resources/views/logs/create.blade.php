@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/calendar.css') }}">
@endsection
@section('content')
<div id="calendar-section">
	<div class="row">
		<div id="calendar-breadcrumb-section" class="col-12">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb bg-light">
			  	<li class="breadcrumb-item active" aria-current="page">工作日誌管理</li>
			    <li class="breadcrumb-item">工作日誌列表</li>
			    <li class="breadcrumb-item active" aria-current="page">新增工作日誌</li>
			  </ol>
			</nav>
		</div>
	</div>
	<div id="calendar-content-section">
		<div class="col-12 content-section">
			<div class="row">
				<div class="form-hint col-12 col-xl-3">
					<h4><b>新增工作日誌</b></h4>
					<p>所有使用者都可以觀看您建立的工作日誌</p>
				</div>
				<form method="POST" class="col-12 col-xl-8" action="{{route('log.store')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group col-12">
					  <label>標題 <a class="text-danger">*</a></label>
					  <input type="text" name="title" class="form-control" placeholder="標題" value="{{old('title')}}" required>
					  @if($errors->has('title'))
					  	<small class="form-text text-danger">{{$errors->first('title')}}</small>
					  @else
					  	<small class="form-text text-muted">標題最多50個字</small>	
					  @endif
					</div>	
					<div class="form-inline col-12 mb-4 date-select-section">
						<input type="date" name="startDate" class="form-control" id="calendar-date-input" value="{{ old('startDate') ? old('startDate') : date('Y-m-d') }}" required>
						<input type="time" name="startTime" class="form-control ml-1 mr-2" id="calendar-time-input" value="{{ old('startTime') ? old('startTime') : date('H:i') }}" required>
						<label>到</label>
						<input type="date" name="endDate" class="form-control calendar-date-input ml-2 mr-1" id="calendar-date-input" value="{{ old('endDate') ? old('endDate') : date('Y-m-d') }}" required>
						<input type="time" name="endTime" class="form-control" id="calendar-time-input" value="{{ old('endTime') ? old('endTime') : date('H:i', strtotime('+5 min')) }}" required>
						@if($errors->has('time'))
							<small class="form-text mt-1 text-danger">{{$errors->first('time')}}</small>
						@endif
					</div>			
					<hr>

					<div class="form-group col-12">
					  <label>狀態 <a class="text-danger">*</a></label>
					  <select name="color_id" class="form-control" required>
					  	@foreach($colors as $color)
					  		<option value="{{$color->id}}" {{($color->id == old('color_id')) ? 'selected' : ''}} >{{$color->name}}</option>
					  	@endforeach
					  </select>
					  @if($errors->has('color_id'))
					  	<small class="form-text text-danger">{{$errors->first('color_id')}}</small>
					  @else
					  	<small class="form-text text-muted">狀態將會以不同的顏色呈現</small>
					  @endif
					</div>
					<div class="form-group col-12">
					  <label>群組</label>
					  <select name="group_id" class="form-control" required>
					  	<option value="0">---</option>
					  	@foreach($groups as $group)
					  		<option value="{{$group->ownerGroup->id}}" {{($group->ownerGroup->id == old('group_id')) ? 'selected' : ''}} >{{$group->ownerGroup->name}}</option>
					  	@endforeach
					  </select>
					  @if($errors->has('group_id'))
					  	<small class="form-text text-danger">{{$errors->first('group_id')}}</small>
					  @else
					  	<small class="form-text text-muted">小組成員的個人行事曆將會出現此工作日誌</small>
					  @endif
					</div>
					
					<div class="form-group col-12">
					  <label>工作日誌內容</label>
					  <textarea name="content" class="form-control" rows="10"></textarea>
					  @if($errors->has('content'))
					  	<small class="form-text text-danger">{{$errors->first('content')}}</small>
					  @endif
					  <br>
					  <small class="form-text text-danger">*工作日誌送出一週後即無法再更改內容</small>
					</div>
					<div class="col-12 mt-5 text-right">
						<button type="submit" class="btn btn-info" >送出</button>
						<a href="{{route('calendar.month.log.list', ['type' => $type,'year' => date('Y'), 'month' => date('m'), 'day' => date('d')])}}" class="btn btn-danger" onclick="return confirm('確定要取消這次的新增嗎? 系統將不會儲存您的資料')">取消</a>
					</div>
				</form>	
			</div>	
		</div>		
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/calendar.js') }}"></script>
@endsection
