@extends('calendar.index')
@section('calendar-item-section')
<div id="calendar-item-detail" class="content-section">
	<div class="mb-4 row">
		<div class="col-6">
			<small><a href="{{route('calendar.month.log.list', ['type' => $type, 'year' => $year, 'month' => $month, 'day' => $day])}}" class="text-muted"><i class="fas fa-chevron-left"></i>&nbsp;&nbsp;返回</a></small>	
		</div>
		<div class="col-6 text-right">
			
			@if( ($log->user_id == Auth::user()->id) )
				<form action="{{route('log.destroy', ['log_id' => $log->id])}}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<a href="{{route('log.edit', ['log_id' => $log->id])}}" class="btn btn-outline-warning"><i class="far fa-edit"></i></a>
					<button onclick="return confirm('確定要刪除嗎?  系統將無法復原您的動作')" class="btn btn-outline-danger"><i class="far fa-trash-alt"></i></button>
				</form>
			@elseif(Auth::user()->id == @$log->ownerGroup->user_id || (!Auth::user()->isGuest()) )
				<form action="{{route('log.destroy', ['log_id' => $log->id])}}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<button onclick="return confirm('確定要刪除嗎?  系統將無法復原您的動作')" class="btn btn-outline-danger"><i class="far fa-trash-alt"></i></button>
				</form>
			@endif
		</div>
	</div>
	<h5><b><i style='color: {{$log->color->color}};' class='fas fa-sticky-note'></i>&nbsp;&nbsp;{{$log->title}}</b></h5>
	<small class="text-muted">{{$log->start_date}} {{date('H:i',strtotime($log->start_time))}} 到 {{$log->end_date}} {{date('H:i',strtotime($log->end_time))}} </small>	
	<br>
	<small><i class="fas fa-user"></i>&nbsp;&nbsp;{{$log->ownerUser->name}}</small>
	@if($log->ownerGroup !== NULL)
		<br>
		<small><i class="fas fa-users"></i>&nbsp;&nbsp;{{$log->ownerGroup->name}}</small>
	@endif
	<hr>
	<div class="text-align-right-and-left">{{$log->content}}</div>
</div>
@endsection