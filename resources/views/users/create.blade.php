@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/user.css') }}">
@endsection
@section('content')

<div id="user-section">
	<div id="user-head-section" class="row">
		<div id="user-breadcrumb-section" class="col-12">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb bg-light">
			  	<li class="breadcrumb-item active" aria-current="page">使用者管理</li>
			    <li class="breadcrumb-item">使用者列表</li>
			    <li class="breadcrumb-item active" aria-current="page">新增使用者</li>
			  </ol>
			</nav>
		</div>
	</div>
	<div id="user-content-section">
		<div class="col-12 content-section">
			<div class="row">
				<div class="form-hint col-12 col-xl-3">
					<h4><b>新增使用者</b></h4>
					<p>只需要輸入姓名、學號、密碼、職稱即可，剩下資料可由使用者後自行更新</p>
				</div>
				<form method="POST" class="col-12 col-xl-6 offset-xl-1" action="{{route('user.store')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group col-12">
					  <label>學號 <a class="text-danger">*</a></label>
					  <input type="text" name="student_id" class="form-control" placeholder="學號" value="{{old('student_id')}}" required>
					  @if($errors->has('student_id'))
					  	<small class="form-text text-danger">{{ $errors->first('student_id') }}</small>
					  @else
					  	<small class="form-text text-muted">學號新增後就不能修改，請務必再三確認</small>
					  	<small class="form-text text-muted">學號最多20個字，限英數字</small>
					  @endif
					</div>
					<div class="form-group col-12">
					  <label>姓名 <a class="text-danger">*</a></label>
					  <input type="text" name="name" class="form-control" placeholder="姓名" value="{{old('name')}}" required>
					  @if($errors->has('name'))
					  	<small class="form-text text-danger">{{ $errors->first('name') }}</small>
					  @else
					  	<small class="form-text text-muted">姓名最多20個字</small>
					  @endif
					</div>
					

					<div class="form-group col-12">
					  <label>密碼 <a class="text-danger">*</a></label>
					  <input type="password" name="password" class="form-control" placeholder="密碼" required>
					  @if($errors->has('password'))
					  	<small class="form-text text-danger">{{ $errors->first('password') }}</small>
					  @else
					  	<small class="form-text text-muted">密碼最少要6個字</small>
					  @endif
					</div>

					<div class="form-group col-12">
					  <label>密碼二次確認 <a class="text-danger">*</a></label>
					  <input type="password" id="password-confirm" name="password_confirmation" class="form-control" placeholder="請再輸入一次密碼" required>
					</div>

					<div class="form-group col-12">
					  <label>職稱 <a class="text-danger">*</a></label>
					  <select name="title" class="form-control" required>
						@foreach($jobTitles as $title)
							<option value="{{$title->id}}" {{ $title->id == old('title') ? 'selected' : '' }}>{{$title->name}}</option>
						@endforeach
					  </select>
					  @if($errors->has('title'))
					  	<small class="form-text text-danger">{{ $errors->first('title') }}</small>
					  @endif
					</div>
					<div class="form-group col-12">
					  <label>Email</label>
					  <input type="email" name="email" class="form-control" placeholder="電子信箱" value="{{old('email')}}">
					  @if($errors->has('email'))
					  	<small class="form-text text-danger">{{ $errors->first('email') }}</small>
					  @else
					  	<small class="form-text text-muted">電子信箱最多100個字</small>
					  @endif
					</div>
					<div class="form-group col-12">
					  <label>連絡電話</label>
					  <input type="text" name="phone" class="form-control" placeholder="連絡電話" value="{{old('phone')}}">	
					  @if($errors->has('phone'))
					  	<small class="form-text text-danger">{{ $errors->first('phone') }}</small>
					  @else
					  	<small class="form-text text-muted">聯絡電話共11個字，請輸入行動電話，ex: (09XX-XXXXXX)</small>
					  @endif			  
					</div>
					<div class="form-group col-12">
					  <label>生日</label>
					  <input type="date" name="birth" class="form-control" value="{{old('birth')}}">
					  @if($errors->has('birth'))
					  	<small class="form-text text-danger">{{ $errors->first('birth') }}</small>
					  @endif
					</div>
					<div class="col-12 mt-5 text-right">
						<button type="submit" class="btn btn-info">送出</button>
						<a href="{{route('user.index')}}" class="btn btn-danger" onclick="return confirm('確定要取消這次的新增嗎? 系統將不會儲存您的資料')">取消</a>
					</div>
				</form>	
			</div>	
		</div>
		

		
	</div>
</div>

@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
@endsection