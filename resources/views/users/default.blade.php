<html>
<head>
    <title>E527 工作日誌系統</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
</head>
<body>
<div class="container">
    <br><br>
    @foreach($arr_groups as $group)
        <div class="row">
            <div class="col-12">
                <h4>群組{{ $loop->iteration }}: {{ $group["name"] }}</h4>
                <small><b>組長</b>: {{ $users[$group["user_id"]-1]->name }}，共{{ count($arr_groupMembers[$loop->index]) }}人</small>
                <br>
                <small><b>描述</b>: {{ $group["description"] }}</small>
            </div>
        </div>
        <br>
        <div class="row">
            @foreach($arr_groupMembers[$loop->index] as $groupMemberId)
                <div class="col-12 col-sm-6 col-md-4 pb-4">
                <div class="card">
                  <div class="card-header">
                    <b>{{ $users[$groupMemberId-1]->name }}</b>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item"><b>學號(帳號)</b>: {{ $users[$groupMemberId-1]->student_id }}</li>
                    <li class="list-group-item"><b>密碼</b>: {{ $users[$groupMemberId-1]->password }}</li>
                    <li class="list-group-item"><b>職稱</b>: {{ $users[$groupMemberId-1]->jobTitle->name }}</li>
                  </ul>
                </div>
                </div>
            @endforeach
        </div>
        <hr>
    @endforeach
    <div class="row">
        <div class="col-12 p-4 text-center">
            <a href="{{ route('home') }}">返回系統首頁</a>
        </div>
    </div>
</div>
</body>
</html>