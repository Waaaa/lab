@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/user.css') }}">
@endsection
@section('content')
<div id="user-section">
	<div id="user-head-section" class="row">
		<div id="user-breadcrumb-section" class="col-8">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb bg-light">
			  	<li class="breadcrumb-item active" aria-current="page">使用者管理</li>
			    <li class="breadcrumb-item active" aria-current="page">使用者列表</li>
			  </ol>
			</nav>
		</div>
		<div id="user-setting-section" class="col-4">					
			<div class="btn-group">
			  <a href="{{route('user.profile')}}" class="btn btn-secondary"><i class="fas fa-list"></i>&emsp;個人</a>
			  <a href="{{route('user.index')}}" class="btn btn-info"><i class="fas fa-list"></i>&emsp;實驗室</a>
			</div>
		</div>
	</div>
	<div id="user-content-section">
		@if (Session::has('successMessage'))
			<div class="col-12 alert alert-success" role="alert">
			  {{ Session::get('successMessage') }}
			</div>
		@endif
		@if (Session::has('errorMessage'))
			<div class="col-12 alert alert-danger" role="alert">
			  {{ Session::get('errorMessage') }}
			</div>
		@endif
		
		<table id="user-table" class="table">
			<thead>
				<tr id="user-table-title">
				  <th scope="col">姓名</th>
				  <th scope="col">學號</th>
				  <th scope="col">電子郵件</th>
				  <th scope="col">連絡電話</th>
				  <th scope="col">職稱</th>
				  <!--
				  <th scope="col">生日</th>
			 	  -->
				  
				  @if(Auth::user()->isSuperAdmin() || Auth::user()->isAdmin())
					  <th scope="col">登入權限</th>
					  <th scope="col"></th>
				  @endif
				</tr>
			</thead>
			<tbody id="user-table-content">
				@foreach($users as $user)
					<tr>
						<td>{{$user->name}}</td>
						<td>{{$user->student_id}}</td>
						<td>{{$user->email}}</td>
						<td>{{$user->phone}}</td>
						<td>{{$user->jobTitle->name}}</td>
						@if(Auth::user()->isSuperAdmin() || Auth::user()->isAdmin())
							<td>
								@if($user->permission == "1")
									<i class="fas fa-check"></i>
								@else
									<i class="fas fa-ban"></i>
								@endif
							</td>
							<td>
								@if($user->id != Auth::user()->id)
									<div class="dropdown">
									  <button class="btn btn-info dropdown-toggle" type="button" id="userMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									  	<i class="fas fa-bars"></i>
									  </button>
									  <div class="dropdown-menu" aria-labelledby="userMenuButton">
									  	
									  	<a class="dropdown-item text-danger" onclick="return confirm('確定要對{{$user->name}}進行密碼的重設嗎?')" href="{{route('user.reset.password', ['user_id' => $user->id])}}">重設密碼</a>
									  	@if($user->permission == "1")
									   		<a class="dropdown-item text-danger" onclick="return confirm('確定要停權{{$user->name}}嗎? 停權後該使用者將無法登入')" href="{{route('user.ban', ['user_id' => $user->id])}}">停權</a>
									    @else
									    	<a class="dropdown-item text-info" onclick="return confirm('確定要復權{{$user->name}}嗎? 復權後該使用者將被允許登入系統')" href="{{route('user.allow', ['user_id' => $user->id])}}">復權</a>
									    @endif
									  </div>
									</div>
								@endif
							</td>
						@endif
					</tr>
				@endforeach
			</tbody>
		</table>

		<div class="col-12 pr-0">
	    @if($users->lastPage() > 1)
	      <nav>
	        <ul class="pagination justify-content-end">
	          @if( $users->currentPage() != 1 )
	            <li class="page-item"><a class="page-link" href="{{ $users->url($users->currentPage()-1) }}">Previous</a></li>
	          @endif
	          @for($i = 1 ; $i <= $users->lastPage(); $i++)

	            <li class="page-item {{ ($users->currentPage() == $i) ? 'active' : '' }}"><a class="page-link" href="{{ $users->url($i) }}">{{ $i }}</a></li>
	          @endfor
	          @if( $users->currentPage() != $users->lastPage() )
	            <li class="page-item"><a class="page-link" href="{{ $users->nextPageUrl() }}">Next</a></li>
	          @endif
	        </ul>
	      </nav>
	    @endif
	  </div>
	</div>
</div>
@if(Auth::user()->isSuperAdmin() || Auth::user()->isAdmin())
	<a href="{{route('user.create')}}" class="btn btn-danger float-insert-button"><i class="fas fa-plus"></i></a>
@endif
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
@endsection
