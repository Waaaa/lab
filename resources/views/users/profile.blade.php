@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/user.css') }}">
@endsection
@section('content')
<div id="user-section">
	<div id="user-head-section" class="row">
		<div id="user-breadcrumb-section" class="col-8">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb bg-light">
			  	<li class="breadcrumb-item active" aria-current="page">使用者管理</li>
			    <li class="breadcrumb-item">使用者列表</li>
			    <li class="breadcrumb-item active" aria-current="page">個人資料</li>
			  </ol>
			</nav>
		</div>
		<div id="user-setting-section" class="col-4">					
			<div class="btn-group">
			  <a href="{{ route('user.profile') }}" class="btn btn-info"><i class="fas fa-list"></i>&emsp;個人</a>
			  <a href="{{ route('user.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i>&emsp;實驗室</a>
			</div>
		</div>
	</div>
	<div id="user-content-section">
		@if (Session::has('successMessage'))
			<div class="col-12 alert alert-success" role="alert">
			  {{ Session::get('successMessage') }}
			</div>
		@endif
		@if (Session::has('errorMessage'))
			<div class="col-12 alert alert-danger" role="alert">
			  {{ Session::get('errorMessage') }}
			</div>
		@endif
		<div class="col-12 content-section">
			<div class="row">
				<div class="form-hint col-12 col-xl-3">
					<h4><b>個人資料</b></h4>
					<p>請於此處更新您個人資料</p>
				</div>
				<form method="POST" class="col-12 col-xl-6 offset-xl-1" action="{{route('user.update.profile')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}
					<div class="form-group col-12">
					  <label>學號 <a class="text-danger">*</a></label>
					  <input type="text" name="student_id" class="form-control" placeholder="學號" value="{{ Auth::user()->student_id }}" disabled='true'>
					  @if($errors->profile->has('student_id'))
					  	<small class="text-danger">{{$errors->profile->first('student_id')}}</small>
					  @endif
					</div>

					<div class="form-group col-12">
					  <label>姓名 <a class="text-danger">*</a></label>
					  <input type="text" name="name" class="form-control profile-input" placeholder="姓名" value="{{ old('name') ? old('name') : Auth::user()->name }}" {{$profileFieldStatus}} required>
					  @if($errors->profile->has('name'))
					  	<small class="text-danger">{{$errors->profile->first('name')}}</small>
					  @else
					  	<small class="form-text text-muted">姓名最多20個字</small>	
					  @endif
					</div>
					

					<div class="form-group col-12">
					  <label>職稱 <a class="text-danger">*</a></label>
					  <select name="title" class="form-control profile-input" {{$profileFieldStatus}} required>
					  	@foreach($jobTitles as $title)
							<option value="{{$title->id}}" {{ Auth::user()->title_id == $title->id ? 'selected' : '' }}>{{$title->name}}</option>
						@endforeach
					  </select>
					  @if($errors->profile->has('title'))
					  	<small class="text-danger">{{$errors->profile->first('title')}}</small>
					  @endif
					</div>
					<div class="form-group col-12">
					  <label>Email</label>
					  <input type="email" name="email" class="form-control profile-input" placeholder="電子信箱" value="{{ old('email') ? old('email') :Auth::user()->email }}" {{$profileFieldStatus}}>
					  @if($errors->profile->has('email'))
					  	<small class="text-danger">{{$errors->profile->first('email')}}</small>
					  @else
					  	<small class="form-text text-muted">電子信箱最多100個字</small>	
					  @endif
					</div>
					<div class="form-group col-12">
					  <label>連絡電話</label>
					  <input type="text" name="phone" class="form-control profile-input" placeholder="連絡電話" value="{{old('phone') ? old('phone') :Auth::user()->phone}}" {{$profileFieldStatus}}>	
					  @if($errors->profile->has('phone'))
					  	<small class="text-danger">{{$errors->profile->first('phone')}} ex: (09XX-XXXXXX)</small>
					  @else
					  	<small class="form-text text-muted">聯絡電話共11個字，請輸入行動電話，ex: (09XX-XXXXXX)</small>
					  @endif			   
					</div>
					<div class="form-group col-12">
					  <label>生日</label>
					  <input type="date" name="birth" class="form-control profile-input" value="{{old('birth') ? old('birth') :Auth::user()->birth}}" {{$profileFieldStatus}}>
					  @if($errors->profile->has('birth'))
					  	<small class="text-danger">{{$errors->profile->first('birth')}}</small>
					  @endif
					</div>
					<div class="col-12 mt-5 text-right">
						<button type="button" class="edit-form-button btn btn-warning">修改</button>
						<button type="submit" onclick="return confirm('確定要進行個人資料的修改嗎?')" class="btn btn-info profile-input" {{$profileFieldStatus}}>送出</button>
					</div>
				</form>	
			</div>	
		</div>
		
		<div class="col-12 mt-4 content-section">
			<div class="row">
				<div class="form-hint col-12 col-xl-3">
					<h4><b>修改密碼</b></h4>
					<p>請於此處修改您的密碼</p>
				</div>
				<form method="POST" class="col-12 col-xl-6 offset-xl-1" action="{{route('user.update.password')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}
					<div class="form-group col-12">
					  <label>舊密碼 <a class="text-danger">*</a></label>
					  <input type="password" name="oldPassword" class="form-control" placeholder="舊密碼" required="">
					  @if($errors->password->has('oldPassword'))
					  	<small class="text-danger">{{$errors->password->first('oldPassword')}}</small>
					  @endif
					</div>

					<div class="form-group col-12">
					  <label>新密碼 <a class="text-danger">*</a></label>
					  <input type="password" name="password" class="form-control" placeholder="密碼" required>
					  @if($errors->password->has('password'))
					  	<small class="form-text text-danger">{{ $errors->password->first('password') }}</small>
					  @else
					  	<small class="form-text text-muted">密碼最少要6個字</small>
					  @endif
					</div>

					<div class="form-group col-12">
					  <label>新密碼二次確認 <a class="text-danger">*</a></label>
					  <input type="password" id="password-confirm" name="password_confirmation" class="form-control" placeholder="請再輸入一次密碼" required>
					</div>
					<div class="col-12 mt-5 text-right">
						<button type="submit" onclick="return confirm('確定要進行密碼的修改嗎? 完成後您需要重新登入')" class="btn btn-info" >送出</button>
					</div>
				</form>
			</div>
		</div>
		<!--
		<div class="col-12 mt-4 content-section">
			<div class="row">
				<div class="form-hint col-12 col-xl-3">
					<h4><b>上傳大頭貼</b></h4>
					<p>請於此處上傳大頭貼</p>
				</div>
				<form method="POST" class="col-12 col-xl-6 offset-xl-1" action="#" enctype="multipart/form-data">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}
					<div class="form-group col-12">
					  <label>大頭貼</label>
					  <input type="file"  accept=".jpg" name="image" class="form-control">
					  @if($errors->image->any())
					  	<small class="form-text text-danger">上傳失敗，請再上傳一次</small>
					  @else
					  	<small class="form-text text-muted">請上傳.jpg檔案，臉變形了不負法律責任喔^^</small>
					  	<small class="form-text text-muted">如要移除舊有的大頭貼，請直接送出空表單</small>
					  @endif
					</div>
					<div class="col-12 mt-5 text-right">
						<button type="submit" onclick="return confirm('確定要進行大頭貼的修改嗎?')" class="btn btn-info" >送出</button>
					</div>
				</form>
			</div>
		</div>
		-->
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/style.js') }}"></script>
@endsection