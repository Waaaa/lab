<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('/users/default', 'UserController@defaultData')->name('user.default');

/* 需要登入後才能檢視 */
Route::middleware(['auth'])->group(function () {
	Route::get('/', 'CalendarController@redirect')->name('home');
	
	/* 使用者管理 */
	Route::get('/users', 'UserController@index')->name('user.index');
	Route::get('/users/create', 'Auth\RegisterController@showRegistrationForm')->name('user.create');
	Route::post('/users', 'Auth\RegisterController@register')->name('user.store');
	Route::get('/users/personal', 'UserController@showProfileForm')->name('user.profile');
	Route::patch('/users/personal/profile', 'UserController@updateProfile')->name('user.update.profile');
	Route::patch('/users/personal/password', 'UserController@updatePassword')->name('user.update.password');
	Route::get('/users/{user_id}/reset/password', 'UserController@resetPassword')->name('user.reset.password')->where('user_id', '[0-9]+');
	Route::get('/users/{user_id}/ban', 'UserController@ban')->name('user.ban')->where('user_id', '[0-9]+');
	Route::get('/users/{user_id}/allow', 'UserController@allow')->name('user.allow')->where('user_id', '[0-9]+');
	Route::post('/users/current', 'UserController@returnUser')->name('user.returnUser');
	Route::post('/users/notice', 'UserController@checkNotice')->name('user.notice');

	/* 群組管理 */
	Route::get('/groups', 'GroupController@index')->name('group.index');
	Route::get('/groups/personal', 'GroupController@personal')->name('group.personal');
	Route::get('/groups/create', 'GroupController@create')->name('group.create');
	Route::post('/groups', 'GroupController@store')->name('group.store');
	Route::get('/groups/{group_id}', 'GroupController@show')->name('group.show')->where('group_id', '[0-9]+');
	Route::get('/groups/{group_id}/edit', 'GroupController@edit')->name('group.edit')->where('group_id', '[0-9]+');
	Route::patch('/groups/{group_id}', 'GroupController@update')->name('group.update')->where('group_id', '[0-9]+');
	Route::delete('/groups/{group_id}', 'GroupController@destroy')->name('group.destroy')->where('group_id', '[0-9]+');

	/* 工作日誌 */
	Route::get('/calendar/month/{type}/{year}/{month}', 'CalendarController@index')->name('calendar.month.index')->where('type', '[A-z]+')->where('year', '[0-9]+')->where('month', '[0-9]+');
	Route::get('/calendar/month/{type}/{year}/{month}/{day}', 'CalendarController@list')->name('calendar.month.log.list')->where('type', '[A-z]+')->where('year', '[0-9]+')->where('month', '[0-9]+')->where('day', '[0-9]+');
	//Route::get('/calendar/month/{year}/{month}/{day}/{log_id}', 'CalendarController@showLogById')->name('calendar.show.log')->where('year', '[0-9]+')->where('month', '[0-9]+')->where('day', '[0-9]+')->where('log_id', '[0-9]+');
	Route::get('/logs/create', 'LogController@create')->name('log.create');
	Route::post('/logs', 'LogController@store')->name('log.store');
	Route::get('/logs/{log_id}', 'LogController@show')->name('log.show')->where('log_id', '[0-9]+');
	Route::get('/logs/{log_id}/edit', 'LogController@edit')->name('log.edit')->where('log_id', '[0-9]+');
	Route::patch('/logs/{log_id}', 'LogController@update')->name('log.update')->where('log_id', '[0-9]+');
	Route::delete('/logs/{log_id}', 'LogController@destroy')->name('log.destroy')->where('log_id', '[0-9]+');

	/* 公告 */
	Route::get('/announcements', 'AnnouncementController@index')->name('announcement.index');
	Route::get('/announcements/create', 'AnnouncementController@create')->name('announcement.create');
	Route::post('/announcements', 'AnnouncementController@store')->name('announcement.store');
	Route::get('announcements/{announcement_id}', 'AnnouncementController@show')->name('announcement.show')->where('announcement_id', '[0-9]+');
	Route::get('/announcements/{announcement_id}/edit', 'AnnouncementController@edit')->name('announcement.edit')->where('announcement_id', '[0-9]+');
	Route::patch('announcements/{announcement_id}', 'AnnouncementController@update')->name('announcement.update')->where('announcement_id', '[0-9]+');
	Route::delete('announcements/{announcement_id}', 'AnnouncementController@destroy')->name('announcement.destroy')->where('announcement_id', '[0-9]+');
	Route::get('/announcements/{announcement_id}/notice', 'AnnouncementController@notice')->name('announcement.notice')->where('announcement_id', '[0-9]+');
	Route::post('/announcements/{announcement_id}/confirm', 'AnnouncementController@confirm')->name('announcement.confirm')->where('announcement_id', '[0-9]+');
});